/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;

@Entity(tableName = "sequenceposexref", primaryKeys = {"sid", "pid"}, indices = {@Index(value =
        {"pid", "sid", "ordering"}, unique = true)})
public class SequencePoseXRef {
    @ColumnInfo(name = "sid")
    private int mSid;
    @ColumnInfo(name = "pid")
    private int mPid;
    @ColumnInfo(name = "ordering")
    private int mOrdering;

    // Note: parameters must match fields.
    public SequencePoseXRef(int mSid, int mPid, int mOrdering) {
        this.mSid = mSid;
        this.mPid = mPid;
        this.mOrdering = mOrdering;
    }

    public int getSid() {
        return mSid;
    }

    public void setSid(int sId) {
        mSid = sId;
    }

    public int getPid() {
        return mPid;
    }

    public void setPid(int pId) {
        mPid = pId;
    }

    public int getOrdering() {
        return mOrdering;
    }

    public void setOrdering(int ordering) {
        mOrdering = ordering;
    }
}
