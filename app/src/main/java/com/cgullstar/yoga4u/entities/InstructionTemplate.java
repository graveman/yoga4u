/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.entities;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.cgullstar.yoga4u.R;
import com.cgullstar.yoga4u.activities.FullscreenActivity;
import com.cgullstar.yoga4u.models.MediaPlayerExtension;
import com.cgullstar.yoga4u.models.VideoViewExtension;
import com.cgullstar.yoga4u.models.ViewAnimatorExtension;

import java.io.IOException;
import java.util.List;
import java.util.MissingResourceException;

import static android.content.Context.MODE_PRIVATE;
import static com.cgullstar.yoga4u.activities.MainActivity.ESSENTIAL_ONLY;
import static com.cgullstar.yoga4u.activities.MainActivity.FULL_VERBOSITY;
import static com.cgullstar.yoga4u.activities.MainActivity.NAME_ONLY;
import static com.cgullstar.yoga4u.activities.MainActivity.PREFERENCES;


@Entity(tableName = "instruction_template", indices = {@Index(value = {"name"}, unique = true)})
public class InstructionTemplate {
    @Ignore
    String TAG = "InstructionTemplate";
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;
    @ColumnInfo(name = "name")
    private String mName;
    @ColumnInfo(name = "pose_id")
    private int mPoseId;
    /**
     * static (stay in a position for mDuration. The same as having 0 repeats.)
     * dynamic (can be mRepeat[ed] without calling the Instruction again)
     */
    @ColumnInfo(name = "dynamic")
    private boolean mDynamic;
    /**
     * standing sitting headstand
     * Will this be used?
     */
    @ColumnInfo(name = "type")
    private String mType;
    @ColumnInfo(name = "transition")
    private boolean mTransition;
    @ColumnInfo(name = "essential")
    private boolean mEssential;
    /**
     * Unless muted, at least this Instruction will be played. Should contain only the name of
     * the pose.
     */
    @ColumnInfo(name = "isName")
    private boolean mIsName;

    // TODO==060 Why Integer instead of int?
    @ColumnInfo(name = "overview_video_id")
    private Integer mOverviewVideoId;
    /**
     * When within a Pose's duration to start playing the overviewVideo (in
     * milliseconds).
     */
    @ColumnInfo(name = "start_time")
    private int mStartTime;
    /**
     * This is the duration of a single unit of instructions (open to interpretation).
     * A part of a pose should usually take the same amount of time, but can be modified by user
     * preferences. A variant will not have its own duration because the purpose of a variant is
     * mainly to facilitate different languages. If, for example, an instructor wants to take more
     * time to get into a pose (transition_in part), then a new template (with its own video and
     * duration) should be made. Keeps things a bit more simple.
     */
    @ColumnInfo(name = "duration")
    private int mDuration;
    /**
     * Times to repeat a dynamic pose
     */
    @ColumnInfo(name = "repeat")
    private int mRepeat;

    /**
     * Note: starts at 0.
     */
    @ColumnInfo(name = "priority")
    private int mOrder;

    /**
     * Whether this template is used to state the name of the Pose, instead of any particular
     * instructions. This should usually be the first Instruction within a Pose, but this is not
     * forced.
     */
    @ColumnInfo(name = "announce_pose")
    private boolean mAnnouncePose;

    @Ignore
    private VideoViewExtension mVideoView;
    @Ignore
    private InstructionVariant mInstructionVariant;
    // TODO==999 multiple languages, so allow multiple isNames to be played (HUH?).
    @Ignore
    private Pose mPose;
    @Ignore
    private List<InstructionTemplate> mSiblings;
    @Ignore
    private static Sequence mSequence;
    @Ignore
    private Context mContentContext;
    @Ignore
    private MediaPlayer mMediaPlayer;
    @Ignore
    private int mRemainingDuration;
    @Ignore
    private Runnable mShowNextViewRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "mShowNextViewRunnable#run");
            // Stop currently playing
            try {
                if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                    Log.d(TAG, "#run Stopping mediaPlayer");
                    mMediaPlayer.stop();
                }
            } catch (IllegalStateException e) {
                Log.e("InstructionTemplate", "MediaPlayer was in an illegal state. It has " +
                        "probably been reset already.");
            }
            // Suspend after the current Instruction's duration.
            int contentIndex =
                    mSequence.getContentView().getIndexByTemplate(InstructionTemplate.this);
            int childCount = mSequence.getContentView().getChildCount();
            if (contentIndex >= childCount - 1) {
                mSequence.suspend();
                return;
            }

            Pose pose = getPose();
            // Note that getNext moves to the next Pose if when hitting the last InstructionTemplate
            // in a Pose.
            InstructionTemplate mActiveTemplate = getNext();
            if (mActiveTemplate == null) {
                // TODO==998A option to fling back to the "main template" (i.e. when you are in down
                //  dog, instead of transitioning into it) (new field?) of the pose instead of the
                //  first template (to avoid long transition_in templates). Flinging to the next
                //  template already works, however, and might be enough.
                mActiveTemplate = pose.getFirstTemplate();
                Log.d(TAG, "Template was NULL but is now set to: " + mActiveTemplate.getName());
            } else {
                Log.d(TAG, "mActiveTemplate: " + mActiveTemplate.getName());
            }
            pose.setActiveTemplate(mActiveTemplate);
            Log.d(TAG, "previous pose name: " + getName());
            // Continue with the next template:
            mActiveTemplate.start(mContentContext);
            Log.d(TAG, "Loaded DisplayedChild: " + mSequence.getContentView().getDisplayedChild());
        }
    };

    @Ignore
    private List<InstructionVariant> mInstructionVariants;
    @Ignore
    private static int mNonEssentialCumulative;
    @Ignore
    private static int mEssentialsDuration;
    @Ignore
    private static int mNonEssentialPlayable;

    /**
     * See {@link InstructionTemplateBuilder}
     *
     * @param id
     * @param name
     * @param poseId
     * @param essential
     * @param overviewVideoId
     * @param order
     * @param duration
     * @param announcePose
     */
    public InstructionTemplate(int id, String name, int poseId, boolean essential,
                               Integer overviewVideoId,
                               int order, int duration, boolean announcePose) {
        mId = id;
        mName = name;
        mPoseId = poseId;
        mEssential = essential;
        mOverviewVideoId = overviewVideoId;
        mOrder = order;
        mDuration = duration;
        mAnnouncePose = announcePose;
    }

    public boolean isMDynamic() {
        return mDynamic;
    }

    public void setMDynamic(boolean dynamic) {
        mDynamic = dynamic;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int duration) {
        mDuration = duration;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getPoseId() {
        return mPoseId;
    }

    public void setPoseId(int poseId) {
        mPoseId = poseId;
    }

    public boolean isTransition() {
        return mTransition;
    }

    public void setTransition(boolean transition) {
        mTransition = transition;
    }

    public boolean isEssential() {
        return mEssential;
    }

    public void setEssential(boolean essential) {
        mEssential = essential;
    }

    public String getName() {
        return mName;
    }

    public boolean isName() {
        return mIsName;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setIsName(boolean name) {
        mIsName = name;
    }

    public Integer getOverviewVideoId() {
        return mOverviewVideoId;
    }

    public void setOverviewVideoId(Integer overviewVideoId) {
        mOverviewVideoId = overviewVideoId;
    }

    public int getStartTime() {
        return mStartTime;
    }

    public void setStartTime(int startTime) {
        mStartTime = startTime;
    }

    public int getRepeat() {
        return mRepeat;
    }

    public void setRepeat(int repeat) {
        mRepeat = repeat;
    }

    public int getOrder() {
        return mOrder;
    }

    public void setOrder(int order) {
        mOrder = order;
    }

    public Pose getPose() {
        return mPose;
    }

    public void setPose(Pose pose) {
        mPose = pose;
    }

    public VideoViewExtension getVideoView() {
        return mVideoView;
    }

    public void setVideoView(VideoViewExtension videoView) {
        mVideoView = videoView;
    }

    /**
     * Gets the remaining duration of the Instruction.
     *
     * @return the remaining duration in milliseconds
     */
    public int getRemainingDuration() {
        int remainingDuration = 0;
        try {
            if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                remainingDuration = mDuration - mMediaPlayer.getCurrentPosition();
            }
        } catch (IllegalStateException e) {
            Log.e("InstructionTemplate", "MediaPlayer did not yield getCurrentPosition. " +
                    "It has probably been reset already.");
        }
        return remainingDuration;
    }

    public void setRemainingDuration(int remainingDuration) {
        mRemainingDuration = remainingDuration;
    }

    /**
     * Show this InstructionTemplate and show the next (using sequenceHandler) after this.duration.
     * Note that the actual media to be shown is already preloaded at
     * {@link ViewAnimatorExtension#addViewsToAnimator(Pose, FullscreenActivity)}
     *
     * @param contentContext The context?
     */
    public void start(Context contentContext) {
        Log.d(TAG, "#start");
        // TODO==999 really need to find a way to discern between db values and variables. But
        //  let's not use nor write an ORM.
        mContentContext = contentContext;
        // Find this template's index in ViewAnimatorExtension and set the displayed
        //  child (a View which as an image set in a variant) to match this template.
        ViewAnimatorExtension contentView = mSequence.getContentView();
        FullscreenActivity fullscreenActivity = contentView.getFullscreenActivity();
        TextView poseNameText = fullscreenActivity.findViewById(R.id.poseNameText);
        poseNameText.setText(mName);
        int contentIndex = contentView.getIndexByTemplate(this);
        contentView.setDisplayedChild(contentIndex);
        Handler sequenceHandler = mSequence.getHandler();
        // If audio isn't playing and mNonEssentialCumulative is too high, this is the place to
        // start debugging. DEBT: when we go back one Instruction (not implemented yet), then the
        // duration of the source Instruction is not subtracted from mNonEssentialCumulative.
        mPose.incrNonEssentialDurationCumulative();
        Log.d(TAG, String.format("Next duration should be: %s", getDuration()));
        // This is for the next instructionTemplate. Note that the first template's duration is set
        // here.
        // TODO==999 Check if this.getAudioID returns an audio file instead of any other type.
        //  During development sometimes another file (video) got returned, probably because of
        //  the refs not matching the values.
        int instructionVariantVideoId = mInstructionVariant.getVideoId();
        SharedPreferences settings = contentContext.getSharedPreferences(
                PREFERENCES, MODE_PRIVATE);
        int verbosity = settings.getInt("verbosity", FULL_VERBOSITY);
        // cumulative includes this.
        if (mEssentialsDuration == 0) {
            initEssentialsDuration();
        }

        mNonEssentialCumulative = mPose.getNonEssentialDurationCumulative();
        Log.d(TAG, "start: mNonEssentialCumulative: " + mNonEssentialCumulative);
        MediaPlayerExtension mediaPlayer = contentView.getFullscreenActivity()
                .getActivityMediaPlayer();
        // Undo audio mute because of a previous Instruction which muted audio due to user
        // preferences.
        if (!fullscreenActivity.isMuted()) {
            mediaPlayer.setVolume(1f, 1f);
        }

        // Case: video
        if (!contentView.getFullscreenActivity().isNoVideo()
                && instructionVariantVideoId != 0) {
            Log.d(TAG, "Start Video...");
            VideoViewExtension videoView = (VideoViewExtension) contentView.getChildAt(
                    contentView.getDisplayedChild());
//                //TODO==010 Also be able to play audio alongside video
            videoView.setOnInfoListener((mp, what, extra) -> {
                // Ensure the previous mediaPlayerExtension has stopped
                try {
                    if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                        Log.d(TAG, "#setOnInfoListener Stopping mediaPlayerExtension");
                        mMediaPlayer.stop();
                        mMediaPlayer.reset();
                    }
                } catch (IllegalStateException e) {
                    Log.d(TAG, "#setOnInfoListener MediaPlayer didn't want " +
                            "to stop.");
                }
                // NOTE: mediaplayer of video and image are the same. In the future, they should
                // both represent the same thing: spoken instructions. If you want music, put it
                // on a different mediaplayer. If you want an extra chime, or ambiend background
                // noises, put each of those on a different mediaplayer as well.
                mMediaPlayer = mp;
                // https://developer.android.com/reference/android/media/MediaPlayer.html#
                // MEDIA_INFO_VIDEO_RENDERING_START
                return true;
            });
            // TODO==050 global mute should probably interact here somewhere
            // Filter when to play the audio or not depending on user preferences verbosity level.
            if (verbosity == -999
                    || (verbosity == NAME_ONLY && !mAnnouncePose)
                    || (verbosity == ESSENTIAL_ONLY && !mEssential)) {
                Log.d(TAG, "mute video");
                mediaPlayer.setVolume(0f, 0f);
            }
            if (mNonEssentialCumulative <= mNonEssentialPlayable * verbosity / 100) {
                // TODO==099 check if video exists
                videoView.resume();
                videoView.start();
            }
        } else {
            // Case: image only mode (noVideo)
            Uri uri =
                    Uri.parse("android.resource://" + fullscreenActivity.getPackageName() + "/" +
                            mInstructionVariant.getAudioId());
            try {
                Log.d(TAG, "Audio preparing...");
                mediaPlayer.reset();
                mediaPlayer.setDataSource(contentContext, uri);
                mediaPlayer.prepare();
            } catch (IOException e) {
                // https://stackoverflow.com/a/55501790
                // called in state 16 MEDIA_PLAYER_STARTED
                e.printStackTrace();
            } catch (IllegalStateException e) {
                // called in state 8 MEDIA_PLAYER_PREPARED
                e.printStackTrace();
            }

            mediaPlayer.setOnPreparedListener(mp -> {
                mMediaPlayer = mp;
                Log.d(TAG, "Trying to play sound");
                // TODO==050 global mute should probably interact here somewhere
                // TODO==050 Randomize which template to play.
                // TODO==050 randomize which variant to play (maybe elsewhere?).

                // Audio comes from the variant which is related to the template tagged with
                // announcePose.
                if (verbosity == -999
                        || verbosity == NAME_ONLY && !mAnnouncePose
                        || verbosity == ESSENTIAL_ONLY && !mEssential) {
                    Log.d(TAG, "Not playing audio.");
                } else if (mEssential
                        || mNonEssentialCumulative <= mNonEssentialPlayable * verbosity / 100) {
                    Log.d(TAG, "mediaPlayer starting.");
                    mediaPlayer.start();
                }
            });
        }
        // Queue the next view/instructions:
        sequenceHandler.postDelayed(mShowNextViewRunnable, getDuration());
        // TODO=99 Add mute feature
    }

    public List<InstructionTemplate> getSiblings() {
        return mSiblings;
    }

    /**
     * mSiblings holds all related {@link InstructionTemplate} INCLUDING SELF.
     * At population time this will hold all templates. After checking how many instructions fit
     * in the duration of the Pose, and in addViewsToAnimator this list will be trimmed down to
     * only the templates which are to be shown.
     *
     * @param siblings List of InstructionTemplates with the same mPoseId, INCLUDING SELF.
     */
    public void setSiblings(List<InstructionTemplate> siblings) {
        if (siblings != null) mSiblings = siblings;
    }

    public void resume() {
        Log.d(TAG, "#resume");
        Log.d(TAG, "mRemainingDuration = " + mRemainingDuration);
        Handler sequenceHandler = mSequence.getHandler();
        sequenceHandler.postDelayed(mShowNextViewRunnable, mRemainingDuration);
    }

    public InstructionVariant getInstructionVariant() {
        return mInstructionVariant;
    }

    public void setInstructionVariant(InstructionVariant instructionVariant) {
        mInstructionVariant = instructionVariant;
    }

    public Sequence getSequence() {
        return mSequence;
    }

    public static void setSequence(Sequence sequence) {
        mSequence = sequence;
    }

    /**
     * {@link Pose#next()} will return the first InstructionTemplate of the next {@link Pose} if
     * there is no next InstructionTemplate.
     *
     * @return The next InstructionTemplate or the first InstructionTemplate of the next
     * {@link Pose}.
     */
    public InstructionTemplate getNext() {
        Log.d(TAG, "InstructionTemplate getNext");
        try {
            return mSiblings.get(mOrder + 1);
        } catch (IndexOutOfBoundsException e) {
            Log.d(TAG, "Next Template is NULL, getting next Pose.");
            Log.d(TAG, "========NEXT POSE========");
            mNonEssentialCumulative = 0;
            initEssentialsDuration();
            return mPose.next().getFirstTemplate();
        }
    }

    private void initEssentialsDuration() {
        // mSiblings consists of ONLY those played, including this.
        mEssentialsDuration = 0;
        for (InstructionTemplate sibling : mSiblings) {
            if (sibling.mEssential) {
                mEssentialsDuration += sibling.mDuration;
            }
        }
        mNonEssentialPlayable = mPose.getDuration() - mEssentialsDuration;
        if (mEssentialsDuration == 0) {
            Log.w(TAG, "No essential instructions found!");
        }
    }

    /**
     * Get the {@link InstructionVariant} with the highest priority belonging to this
     * {@link InstructionTemplate}.
     * Set user specified language variant OR fallback to English.
     * TODO==099 incorporate more user settings, so the priority variant is most suited to the
     * user.
     *
     * @param context The context activity, provides user settings.
     * @return InstructionVariant
     */
    public InstructionVariant getPriorityVariant(FullscreenActivity context) {
        InstructionVariant priorityVariant = null;
        SharedPreferences settings = context.getSharedPreferences(
                PREFERENCES, MODE_PRIVATE);
        int userLanguageId = settings.getInt("languageId", 1);
        for (InstructionVariant variant : mInstructionVariants) {
            // Case: specific variant which suits the user (language, ...)
            if (variant.getLanguageId() == userLanguageId) {
                priorityVariant = variant;
                break;
                // TODO==020 use constant for default language instead of 1
                // Fall back to default language. May be overridden in the next iteration
            } else if (priorityVariant == null && variant.getLanguageId() == 1) {
                priorityVariant = variant;
            }
        }
        if (priorityVariant == null) {
            throw new MissingResourceException(
                    "No default InstructionVariant found! " +
                            "Ensure an InstructionVariant with default language en_us exists.",
                    "InstructionVariant",
                    "languageId==1");
        }
        return priorityVariant;
    }

    public void setInstructionVariants(List<InstructionVariant> instructionVariants) {
        mInstructionVariants = instructionVariants;
    }

    public MediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        mMediaPlayer = mediaPlayer;
    }

    public boolean isAnnouncePose() {
        return mAnnouncePose;
    }

    public void setAnnouncePose(boolean announcePose) {
        mAnnouncePose = announcePose;
    }
}
