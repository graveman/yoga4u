/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "instructor", indices = {@Index(value = {"name", "code"},
        unique = true)})
public class Instructor {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "name")
    private final String mName;
    @ColumnInfo(name = "code")
    private final String mCode;
    @ColumnInfo(name = "gender")
    private String mGender;

    public Instructor(String name, String code) {
        mName = name;
        mCode = code;
    }
//    private final String mStyleIds; // TODO==100 InstructorStyleXRef
//    private final String mLanguageIds; TODO==100 InstructorLanguagesXRef
}
