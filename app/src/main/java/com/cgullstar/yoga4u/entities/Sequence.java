/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.entities;

import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.cgullstar.yoga4u.activities.FullscreenActivity;
import com.cgullstar.yoga4u.interfaces.MediaView;
import com.cgullstar.yoga4u.models.ImageViewExtension;
import com.cgullstar.yoga4u.models.VideoViewExtension;
import com.cgullstar.yoga4u.models.ViewAnimatorExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;


/**
 * A series of {@link Pose}
 */
@Entity(tableName = "sequence", indices = {@Index(value = {"name"}, unique = true)})
public class Sequence {
    @Ignore
    String TAG = "Sequence";
    @Ignore
    private int mStopPosition;
    @Ignore
    private boolean mIsSuspended;

    ViewAnimatorExtension getContentView() {
        return mContentView;
    }

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    public Sequence(int id, @NonNull String name) {
        mId = id;
        mName = name;
    }

    @ColumnInfo(name = "name")
    private String mName;
    @Ignore
    private int mPosesCount;
    @Ignore
    private boolean mPaused = false;
    @Ignore
    private Handler mHandler;
    @Ignore
    private ViewAnimatorExtension mContentView;
    @Ignore
    private List<Pose> mPoses;

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getPosesCount() {
        return mPosesCount;
    }

    public void setPosesCount(int posesCount) {
        mPosesCount = posesCount;
    }

    public boolean isPaused() {
        return mPaused;
    }

    public void setPaused(boolean paused) {
        mPaused = paused;
    }

    public Handler getHandler() {
        return mHandler;
    }

    public void setHandler(Handler handler) {
        mHandler = handler;
    }

    public void setContentView(ViewAnimatorExtension contentView) {
        mContentView = contentView;
    }

    /**
     * Prepare a Handler and start playing the Sequence.
     *
     * @return true on success
     */
    public boolean start() {
        Log.d(TAG, "=====start=====");
        // TODO==020 go from loading screen to sequence (can use dummyview as a loading screen)
        mContentView.setSequence(this);
        mHandler = new Handler();
        mContentView.setDisplayedChild(1);
        // Skip dummy and any other buttons or views which are not part of the instructions.
        for (int i = 0; i < mContentView.getChildCount(); i++) {
            if ((mContentView.getChildAt(i) instanceof MediaView)) {
                playMediaViewAt(i, mContentView);
                return true;
            }
        }
        throw new IllegalStateException("No ImageViewExtension views found, this might mean that " +
                "there are no Poses defined for the Sequence.");
    }

    /**
     * Pauses audio and video playback through the mediaplayer associated with the VideoView or
     * FullscreenActivity (in the case of image only mode).
     */
    public void pause() {
        // TODO==200 user option to select restart pose or resume from pause on double tap.
        // TODO==999 on double tap video crashes sometimes(IllegalStateException, setDataSource)
        Log.d(TAG, "sequence =====pause====");
        FullscreenActivity fullscreenActivity = mContentView.getFullscreenActivity();
        // Pause the video.
//        if (!fullscreenActivity.isNoVideo()) {
        MediaView mediaView =
                (MediaView) mContentView.getChildAt(mContentView.getDisplayedChild());
        MediaPlayer mp = mediaView.getInstructionTemplate().getMediaPlayer();
        assert mp != null;
        if (mp == null) {
            // Expect mediaPlayer to be set either at
            throw new NullPointerException("Sequence#pause: MediaPlayer not set properly.");
        }
        mStopPosition = mp.getCurrentPosition();
        Log.d(TAG, "mStopPosition = " + mStopPosition);
        InstructionTemplate instructionTemplate = mediaView.getInstructionTemplate();
        int remainingDuration = instructionTemplate.getDuration() - mStopPosition;
        mediaView.getInstructionTemplate().setRemainingDuration(remainingDuration);
        try {
            if (mediaView.canPause()) {
                Log.d(TAG, "pause mediaView");
                mp.pause();
            } else {
                Log.e(TAG, "mediaView could not pause!");
                Log.w(TAG, "mediaView could not pause!");
                mediaView.stop();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.d(TAG, "mediaView was not set!");
        }
//        }
//        }
        mContentView.cleanup();
        mPaused = true;
    }

    public void resumeMediaView(int whichChild, ViewAnimatorExtension mContentView) {
        // TODO==11A (DEPENDS on 10A) move functionality to Sequence.start() (play???)
        Log.d(TAG, "========resume========");
        MediaView mediaView =
                (MediaView) mContentView.getChildAt(mContentView.getDisplayedChild());
        MediaPlayer mp = mediaView.getInstructionTemplate().getMediaPlayer();
        if (mp != null) {
            mp.start();
        }
        Pose pose = getPose(whichChild);
        if (pose != null) {
            mPaused = false;
            //  Continue with the actual timings of the instructions, instead of just
            //  continuing to play the video until the very end.
            //  So we need to store the position of the video when pausing, then do a callback with
            //  the remaining time.
            InstructionTemplate instructionTemplate = mediaView.getInstructionTemplate();
//            InstructionTemplate instructionTemplate = pose.getActiveTemplate();
            instructionTemplate.setMediaPlayer(mp);
            instructionTemplate.resume();
        } else {
            throw new IllegalStateException("Could not find pose when playing media view " +
                    "(probably on resuming on doubletap, otherwise on start)." +
                    ".");
        }
    }

    /**
     * Start playback from the specified Image- or Video View.
     *
     * @param whichChild   child (ImageView or VideoView of ContentView(?). Starts at 0
     * @param mContentView View which holds the childviews
     */
    public void playMediaViewAt(int whichChild, ViewAnimatorExtension mContentView) {
        // TODO==11A (DEPENDS on 10A) move functionality to Sequence.start() (play???)
        Log.d(TAG, "========play========");
        // TODO==999 code smell
        Pose pose = getPose(whichChild);
        if (pose != null) {
            setPaused(false);
            pose.getActiveTmpl().start(mContentView.getContext());
        } else {
            throw new IllegalStateException("Could not find pose when playing media view " +
                    "(probably on resuming on doubletap, otherwise on start)." +
                    ".");
        }
    }

    /**
     * At the end of the {@link Sequence}. Prevent Sequence from looping back to start.
     * TODO==050: go to finish/end/summary/congratulations screen (duplicate TODO).
     */
    public void suspend() {
        Log.d(TAG, "=====suspend====");
        MediaView mediaView =
                (MediaView) mContentView.getChildAt(mContentView.getDisplayedChild());
        MediaPlayer mp = mediaView.getInstructionTemplate().getMediaPlayer();
        try {
            if (mp != null && mp.isPlaying()) {
                Log.d(TAG, "MediaPlayer Was still playing. Stopping");
                mp.stop();
                mp.reset();
            }
        } catch (IllegalStateException e) {
            Log.e(TAG, "MediaPlayer was in an illegal state. It has " +
                    "probably been reset already.");
        }
        mContentView.cleanup();
        mIsSuspended = true;
    }

    private Pose getPose(int whichChild) {
        Pose pose;
        List<Class> mediaViewClasses = new ArrayList<Class>(2);
        mediaViewClasses.addAll(Arrays.asList(ImageViewExtension.class, VideoViewExtension.class));
        // < 2 because 1 is the dummy view, and we need at least 1 more view to do anything.
        if (mContentView.getChildCount() < 2) {
            pose = null;
        } else if (
                mediaViewClasses.contains(mContentView.getChildAt(whichChild).getClass())) {
            MediaView mediaView = (MediaView) mContentView.getChildAt(whichChild);
            pose = mediaView.getPose();
        } else {
            pose = null;
        }
        return pose;
    }

    public List<Pose> getPoses() {
        return mPoses;
    }

    final public void setPoses(List<Pose> poses) {
        // Allow to set only once, because we must ensure the order remains the same for Pose#next
        if (mPoses != null) {
            throw new IllegalStateException("setPoses was already called");
        }
        mPoses = Objects.requireNonNull(poses);
    }

    public boolean isSuspended() {
        return mIsSuspended;
    }

    public void setSuspended(boolean suspended) {
        mIsSuspended = suspended;
    }
}
