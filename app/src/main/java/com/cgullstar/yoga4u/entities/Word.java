/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "word_table", indices = @Index(value = {"word"}, unique = true))
public class Word {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;

    public Word(@NonNull String word) {
        mWord = word;
    }

    public int getId() {
        return mId;
    }

    @NonNull
    @ColumnInfo(name = "word")
    private String mWord;

    public void setId(int id) {
        mId = id;
    }

    public String getWord() {
        return mWord;
    }
}
