/*
 * Copyright 2019 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

//package com.cgullstar.yoga4u.entities;

// TODO==099 fix this https://developer.android.com/training/data-storage/room/relationships#many-to-many
// error: pId, iId referenced in the index does not exists in the Entity. Available column names:pId, iId

// The column iId in the junction entity com.cgullstar.yoga4u.entities.PoseInstructionTemplateXRef is being used to resolve a relationship but it is not covered by any index. This might cause a full table scan when resolving the relationship, it is highly advised to create an index that covers this column.
//@Entity(tableName = "poseinstructiontemplatexref", primaryKeys = {"pId", "iId"})

//@Entity(tableName = "poseinstructiontemplatexref", primaryKeys = {"pId", "iId"}, indices =
//        {@Index(value = {"pId", "iId"}, unique = true)})
//public class PoseInstructionTemplateXRef {
//
//
//    public PoseInstructionTemplateXRef(@NonNull Integer pId, @NonNull Integer iId){
//        this.iId = iId;
//        this.pId = pId;
////        this.temptest = temptest;
//    }
//
//    @NonNull
//    private Integer pId;
//
//    @NonNull
//    private Integer iId;
//
////    private int temptest;
//
//    public Integer getPId() {
//        return pId;
//    }
//
//    public void setPId(Integer pId) {
//        this.pId = pId;
//    }
//
//    public Integer getIId() {
//        return iId;
//    }
//
//    public void setIId(Integer iId) {
//        this.iId = iId;
//    }
//
////    public int getTemptest() {
////        return temptest;
////    }
////    public void setTemptest(Integer temptest) {
////        this.temptest = temptest;
////    }
//}
