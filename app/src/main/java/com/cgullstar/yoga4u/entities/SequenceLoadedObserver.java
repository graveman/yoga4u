/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.entities;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.Lifecycle;

import com.cgullstar.yoga4u.activities.FullscreenActivity;

import static androidx.lifecycle.Lifecycle.State.STARTED;

public class SequenceLoadedObserver {
    String TAG = "SequenceLoadedObserver";
    private final Context mContext;
    private Lifecycle mLifecycle;
    private boolean mStarted;

    public SequenceLoadedObserver(Context context, Lifecycle lifecycle) {
        mContext = context;
        mLifecycle = lifecycle;
    }

    /**
     * The notify method of the Observer Pattern. This will start the yoga sequence.
     */
    public void update() {
        if (!mStarted && mLifecycle.getCurrentState().isAtLeast(STARTED)) {
            Log.d(TAG, "update");
            FullscreenActivity fullscreenActivity = (FullscreenActivity) mContext;
            Sequence sequence = fullscreenActivity.getSequence();
            mStarted = sequence.start();
        } else {
            throw new IllegalStateException("App has not started fully yet!");
        }
    }
}
