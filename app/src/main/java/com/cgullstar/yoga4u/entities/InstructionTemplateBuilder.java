/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.entities;

/**
 * https://stackoverflow.com/a/19719701
 */
public class InstructionTemplateBuilder {
    private int mId = 0;
    private String mName = "";
    private int mPoseId = 0;
    private boolean mEssential = false;
    private int mOverviewVideoId = 0;
    private int mOrder = 0;
    private int mDuration = 0;
    private boolean mAnnouncePose = false;

    public InstructionTemplateBuilder setRequiredFields(int id, String name, int poseId,
                                                        boolean essential,
                                                        int order, int duration) {
        mId = id;
        mName = name;
        mPoseId = poseId;
        mEssential = essential;
        mOrder = order;
        mDuration = duration;
        return this;
    }

    /**
     * @param announcePose If true, this Instruction is intended to announce the name of the Pose.
     * @return True if this Instruction is intended to anounce the name of the Pose.
     */
    public InstructionTemplateBuilder setAnnouncePose(boolean announcePose) {
        mAnnouncePose = announcePose;
        return this;
    }

    public InstructionTemplateBuilder setOverviewVideoId(int overviewVideoId) {
        mOverviewVideoId = overviewVideoId;
        return this;
    }

    public InstructionTemplate build() {
        return new InstructionTemplate(mId, mName, mPoseId, mEssential, mOverviewVideoId, mOrder,
                mDuration, mAnnouncePose);
    }
}
