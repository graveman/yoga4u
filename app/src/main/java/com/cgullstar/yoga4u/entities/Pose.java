/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.entities;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.List;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;
import static com.cgullstar.yoga4u.activities.MainActivity.PREFERENCES;


@Entity(tableName = "pose", indices = {@Index(value = {"name"},
        unique = true)})
public class Pose {
    @Ignore
    String TAG = "Pose";
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;
    @ColumnInfo(name = "duration")
    private int mDuration;

    @NonNull
    @ColumnInfo(name = "name")
    private String mName;

    public Pose(int id, @NonNull String name, int duration) {
        mId = id;
        mName = name;
        mDuration = duration;
    }

    @Ignore
    private Sequence mSequence;
    @Ignore
    private List<InstructionTemplate> mInstructionTemplates;
    @Ignore
    private InstructionTemplate mActiveTemplate;
    @Ignore
    private int mInstructionsDurationCumulative;

    public Sequence getSequence() {
        return mSequence;
    }

    public void setSequence(Sequence sequence) {
        mSequence = sequence;
    }

    public List<InstructionTemplate> getInstructionTemplates(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        String userSkill = settings.getString("skill", "beginner");
        // TODO==090 --- pose ----------------------------------- -> next pose
        //  Template.OverViewVideo 1 (essential) [2, 3, ...] - smooth transitions?
        //  Variant audio 1 (essential) [2, 3...] (optional)
        //  Variant video: optional popup views, may be maximized or floating?

        // TODO==090 design algorithm to determine how many audio clips to play
        //  per pose, must fit in length of pose, modified by verbosity, must play at least
        //  essential unless verbose setting is "name only" (difference with "Mute" is that the
        //  pose's name will be said) (essential must not be longer than length of pose at
        //  creation and will be constrained at play time). ...

        // TODO==050 verbosity levels (depends on OSD (todo)):
        //  - no text: do not show pose names, stats, etc. No text on the screen at all.
        //      Separately set spoken instructions.
        // TODO==200 Extra instructions will be added as soon as possible, but evenly spaced out
        //  with even breaks between Instructions.
        // TODO==100Get relevant InstructionVariants for this pose, from InstructionTemplate
        //  (depending on skill, verbosity, time, essentiality);
        //  InstructionVariants are separate from this. After filtering using the user's
        //   preferences, a random one will be chosen (todo).

        // TODO==030 Duration of Pose adjustable in different ways:
        //  1. Absolute per session per Pose (overrides of 2 and 3)
        //  2. Modifier (Based on user preferences: duration of session+skill+...?) (Overrides 3)
        //  3. Absolute defined in Pose definition

        //  TODO==090 A user should not modify default or downloaded sequences, instead, they can
        //   create a duplicate/fork of the sequence and modify that.
        List<InstructionTemplate> instructionTemplates = getInstructionTemplates();
        // TODO==010 check verbosity
        // TODO==010 check focus areas


        // TODO==090 was thinking about dropping InstructionTemplate/Variant and just go
        //  Instruction but really if there's going to be a ton of content having this duality is
        //  going to save a ton of bandwidth.

        return fitTemplateDuration(instructionTemplates);
    }


    /**
     * Get InstructionTemplates for this Pose so they fit in the duration of this Pose.
     * Otherwise (either/or) TODO==200:
     * 1. Error
     * 2. Dump and replace
     * a. pop                  <- implemented
     * b. best fit
     * 3. Ask for user correction
     * <p>
     * TODO==110 What if there isn't enough Instruction duration to fill the duration of a pose?
     * We could:
     * - move to the next pose (sounds good, maybe throw a warning too?)
     * - make certain Instruction types longer (maybe not)
     * - extend instructions proportionally (maybe not)
     * - prompt the user (ugh yagni)
     * - constrain on creation of the pose (TODO==099)
     *
     * @param instructionTemplates List of instructionTemplates to "pop down" until they fit in
     *                             this Pose's duration.
     * @return List of InstructionTemplate filtered to fit in the duration of this Pose [TODO==010
     * make generic] by means of popping.
     */
    private List<InstructionTemplate> fitTemplateDuration(List<InstructionTemplate>
                                                                  instructionTemplates) {
        int templatesDuration = 0;
        for (InstructionTemplate template : instructionTemplates) {
            templatesDuration += template.getDuration();
        }
        while (templatesDuration > mDuration) {
            InstructionTemplate lastTemplate =
                    instructionTemplates.remove(instructionTemplates.size() - 1);
            templatesDuration -= lastTemplate.getDuration();
        }
        return instructionTemplates;
    }

    public List<InstructionTemplate> getInstructionTemplates() {
        return mInstructionTemplates;
    }

    public void setInstructionTemplates(List<InstructionTemplate> instructionTemplates) {
        mInstructionTemplates = instructionTemplates;
    }

    public InstructionTemplate getActiveTmpl() {
        Log.d(TAG, "#getActiveTmpl");
        Log.d(TAG, "pose name: " + mName);
        if (mActiveTemplate != null) {
            Log.d(TAG, "template name: " + mActiveTemplate.getName());
        }
        if (mActiveTemplate == null) {
            Log.d(TAG, "template name: " + getFirstTemplate().getName());
            mActiveTemplate = getFirstTemplate();
        }
        return mActiveTemplate;
    }

    public void setActiveTemplate(InstructionTemplate activeTemplate) {
        Log.d(TAG, "#setActiveTemplate pose name:" + mName);
        if (mActiveTemplate != null) {
            Log.d(TAG, "old active template name:" + mActiveTemplate.getName());
        }
        Log.d(TAG, "new active template name:" + activeTemplate.getName());
        mActiveTemplate = Objects.requireNonNull(activeTemplate);
    }

    /**
     * @return Either the next Pose or the last one.
     */
    public Pose next() {
        List<Pose> poses = mSequence.getPoses();
        for (int i = 0; i < poses.size() - 1; i++) {
            if (poses.get(i) == this) {
                return poses.get(i + 1);
            }
        }
        // Return the last item
        return poses.get(poses.size() - 1);
    }

    public InstructionTemplate getFirstTemplate() {
        return getInstructionTemplates().get(0);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return Objects.requireNonNull(mName);
    }

    public int getDuration() {
        return mDuration;
    }

    public void setDuration(int duration) {
        mDuration = duration;
    }

    /**
     * Increments the cumulative duration of instructions which are not essential. This is used
     * to determine how many additional instructions will be played on top of the already played
     * essential instructions, depending on user preferences.
     */
    public void incrNonEssentialDurationCumulative() {
        if (!mActiveTemplate.isEssential()) {
            mInstructionsDurationCumulative += mActiveTemplate.getDuration();
        }
    }

    public void setInstructionsDurationCumulative(int instructionsDurationCumulative) {
        mInstructionsDurationCumulative = instructionsDurationCumulative;
    }

    public int getNonEssentialDurationCumulative() {
        return mInstructionsDurationCumulative;
    }
}
