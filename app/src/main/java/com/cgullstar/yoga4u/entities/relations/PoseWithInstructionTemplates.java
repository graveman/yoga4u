/*
 * Copyright 2019 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

//package com.cgullstar.yoga4u.entities.relations;
//
//import androidx.annotation.NonNull;
//import androidx.room.Embedded;
//import androidx.room.Junction;
//import androidx.room.Relation;
//
//import com.cgullstar.yoga4u.entities.InstructionTemplate;
//import com.cgullstar.yoga4u.entities.Pose;
//import com.cgullstar.yoga4u.entities.PoseInstructionTemplateXRef;
//
//import java.util.List;

// https://developer.android.com/training/data-storage/room/relationships#many-to-many
// TODO==99999 simplify to m2o: instructions cannot be reused, a pose has multiple Instruction
//  Templates (o2m), Instruction points to just one pose (m2o)
//  Might still want to try to do this later...
//public class PoseWithInstructionTemplates {
//    @Embedded
//    public Pose pose;
//    @Relation(
//            parentColumn = "id",
//            entity = InstructionTemplate.class,
//            entityColumn = "id",
//            associateBy = @Junction(
//                    value = PoseInstructionTemplateXRef.class,
//                    parentColumn = "pId",
//                    entityColumn = "iId")
//    )
//    @NonNull
//    public List<InstructionTemplate> instructions;
//}
