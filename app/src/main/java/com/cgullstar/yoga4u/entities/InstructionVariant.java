/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.entities;

import android.content.Context;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "instruction_variant", indices = {@Index(value = {"instruction_text"},
        unique = true)})
public class InstructionVariant {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mId;
    @ColumnInfo(name = "instruction_template_id")
    private int mInstructionTemplateId;
    // TODO== 9999 change to point to InstructionText object instead (if needed, note Vocals has it
    //  already though!))
    @ColumnInfo(name = "instruction_text")
    private String mInstructionText;
    @ColumnInfo(name = "image_id")
    private int mImageId;
    @ColumnInfo(name = "vocal_id")
    private int mVocalId;
    @ColumnInfo(name = "audio_id")
    private int mAudioId;
    @ColumnInfo(name = "video_id")
    private int mVideoId;
    /**
     * When within an InstructionTemplate's duration to start playing audio, video, and text (in
     * milliseconds).
     */
    @ColumnInfo(name = "start_time")
    private int mStartTime;
    /**
     * This is an optional override of the duration of {@link InstructionTemplate}, but I don't
     * see how this will work, so TODO=050 remove mDuration here.
     */
    @ColumnInfo(name = "duration")
    private int mDuration;
    @ColumnInfo(name = "priority")
    private int mPriority;

    // TODO=999 override min/max break time between Instructions?
    @ColumnInfo(name = "min_skill")
    private int mMinSkill;
    @ColumnInfo(name = "language_id")
    private int mLanguageId;

    public InstructionVariant(int id, String instructionText, int instructionTemplateId,
                              int audioId, int imageId, int videoId, int languageId) {
        mId = id;
        // TODO==9999 refactor back to mInstructionTextId (if we ever need more fields on
        //  InstructionText. For now just having a string on InstructionVariant is fine.)
        mInstructionText = instructionText;
        mInstructionTemplateId = instructionTemplateId;
        mAudioId = audioId;
        mImageId = imageId;
        mVideoId = videoId;
        mLanguageId = languageId;
    }

    public int getAudioId() {
        return mAudioId;
    }

    public void setAudioId(int audioId) {
        mAudioId = audioId;
    }

    public int getDuration() { return mDuration; }

    public void setDuration(int duration) {
        mDuration = duration;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getInstructionTemplateId() { return mInstructionTemplateId; }

    public void setInstructionTemplateId(int instructionTemplateId) {
        mInstructionTemplateId = instructionTemplateId;
    }

    public String getInstructionText() { return mInstructionText; }

    public void setInstructionText(String instructionText) {
        mInstructionText = instructionText;
    }

    public int getImageId() { return mImageId; }

    public void setImageId(int imageId) {
        mImageId = imageId;
    }

    public int getVocalId() { return mVocalId; }

    public void setVocalId(int vocalId) {
        mVocalId = vocalId;
    }

    public int getVideoId() { return mVideoId; }

    public void setVideoId(int videoId) {
        mVideoId = videoId;
    }

    public int getStartTime() { return mStartTime; }

    public void setStartTime(int startTime) {
        mStartTime = startTime;
    }

    public int getPriority() { return mPriority; }

    public void setPriority(int priority) {
        mPriority = priority;
    }

    public int getMinSkill() { return mMinSkill; }

    public void setMinSkill(int minSkill) {
        mMinSkill = minSkill;
    }

    //TODO==020 might be useful?
    public void start(Context context) {
    }

    public int getLanguageId() { return mLanguageId; }

    public void setLanguageId(int languageId) {
        mLanguageId = languageId;
    }
}
