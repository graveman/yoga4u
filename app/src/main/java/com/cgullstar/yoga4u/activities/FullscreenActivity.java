/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.activities;

import android.annotation.SuppressLint;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.cgullstar.yoga4u.BuildConfig;
import com.cgullstar.yoga4u.R;
import com.cgullstar.yoga4u.entities.InstructionTemplate;
import com.cgullstar.yoga4u.entities.Pose;
import com.cgullstar.yoga4u.entities.Sequence;
import com.cgullstar.yoga4u.entities.SequenceLoadedObserver;
import com.cgullstar.yoga4u.entities.relations.SequenceWithPoses;
import com.cgullstar.yoga4u.entities.relations.TemplateWithVariants;
import com.cgullstar.yoga4u.interfaces.MediaView;
import com.cgullstar.yoga4u.models.MediaPlayerExtension;
import com.cgullstar.yoga4u.models.ViewAnimatorExtension;
import com.cgullstar.yoga4u.roomdb.SequenceWithPosesViewModel;
import com.cgullstar.yoga4u.roomdb.TemplateWithVariantsViewModel;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

// Notes
// database data is at /data/data/com.cgullstar.yoga4u/databases

// TODO==001 Background video
// TODO==001 LICENSE page with all 3rd party sources TASL attributed
// TODO==010 https://stackoverflow.com/questions/10264447/java-best-practice-is-declaring-the
//  -constructor-before-the-class-variables-a-b
// TODO==010 A pose retrieves "Instructions" based on:
//  -chosen skill level
//  -verbosity
//  -focus areas (focus on body part, benefit(Ask Anne?)) [low back, core, hip flexor,
//  hamstring, flexibility, chest shoulder, back strength, aerobic]
//  Also it should be possible to have multiple of the same descriptor to add a degree of
//  uniqueness to each session. I.e. instructions on the same pose may be given in multiple
//  different ways, as long as "Essential" instructions are always mentioned. Statistics are
//  kept so that the least called instructions are prioritized. Calling more templates has
//  preference over calling multiple variants of a template. Weights (importance) may be set to
//  influence prioritization, but diversity (options: on, off) in instructions is the
//  default.
//  Pose:
//    -Instruction Template (Fields: Essential, video(overview), [depending on the verbosity
//    setting, more non-essential Instruction Templates will be randomly called])
//      -Instruction Variant  (Fields: audio, video(focus/detail) [iff the template was
//      essential, at least one variant will be called.])

// TODO==010 Also add use preference total session duration, used to calculate how many poses and
//  InstructionTemplates of those poses should be added.
// TODO==010 add bool isTransition to pose? Use type instead.
// TODO==010 Refactor structure like https://github
//  .com/android/sunflower/tree/master/app/src/main/java/com/google/samples/apps/sunflower
//  or like (Java) https://github.com/android/architecture-components-samples/tree/master
//  /BasicSample/app/src/main/java/com/example/android/persistence
// TODO==010 architecture to allow either specific or random Instructions for Poses (in a Sequence)
// TODO==020 smooth transition between videos
// TODO==020 pose fields: level, focus (e.g. core, twists, hip flexor), type (e.g.
//  warmup/general/savasana), hold duration,
// TODO==020 Concept: sequence section (field) e.g. warmup, sun salutations, standing balances,
//  twists, cooldown, savasana
// TODO==020 A Todo file? Make these into GitLab issues?
// TODO==030 document comments (Javadoc) (alt+enter on methods)
// TODO==040 organize files https://github
//  .com/codepath/android_guides/wiki/Organizing-your-Source-Files#organize-packages-by-category
// TODO==040 possible bug: audio delay is longer if going back to :-1 when session was done.
// TODO==050 prevent sleep
// TODO==060 disclaimer
// TODO==060 Make as much immutable (final) as possible, also synchronized when a resource can be
//  accessed by multiple objects and it should be orchestrated (one at a time).
// TODO==060 sequence generator
// TODO==070 sequence/pose/instructions builder
// TODO==080 [FEATURE] pause on app lose focus(?) or play in background
// TODO==080 content providers for media
// TODO==080 slide up down right for audio
// TODO==080 slide up down left for playback speed/pose hold time
// TODO==080 loading screen
// TODO==100 yoga quiz to check if the user isn't hurting themselves
// TODO==100 habit builder/assistant/coach
// TODO==200 [FEATURE] quick sequence editor
// TODO==500 sharing/community
// TODO==500 Add music, bpm to match intensity of section of sequence
// TODO==500 translations
// TODO==500 unit tests
// https://www.videezy.com/free-video/yoga

//https://developer.android.com/reference/androidx/room/Transaction.html
// http://myhexaville.com/2017/02/03/android-smarter-gettersetter-generators

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * https://stackoverflow.com/a/27665019
 */
public class FullscreenActivity extends AppCompatActivity {
    private static final int ACTIONBAR_HIDE_DELAY = 2300;
    String TAG = "FullscreenActivity";
    // TODO==090 display name of the current pose
    //  options:
    //  permanently, temporarily, translations, including sanskrit
    // TODO==091 display duration of current pose
    // TODO==092 display duration of total sequence

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    public ViewAnimatorExtension mContentView;  // View which represents sequence
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    // TODO==010 release this mediaplayer
    private MediaPlayerExtension mActivityMediaPlayer = new MediaPlayerExtension();
    // TODO==070 User define noVideo
    private boolean mNoVideo;
    private int mPosesSetCount;
    private SequenceWithPoses mSequenceWithPoses;
    private ActionBar mActionbar;
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    private GestureDetector mDetector;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
//            ActionBar actionBar = getSupportActionBar();
            if (mActionbar != null) {
//              https://stackoverflow.com/a/38427941
                mActionbar.setBackgroundDrawable(new ColorDrawable(
                        android.graphics.Color.TRANSPARENT));
                mActionbar.show();
            }
        }
    };

    private Sequence mSequence;
    private int mPosesCount;
    private List<SequenceLoadedObserver> mLoadedObserverCollection;

    private boolean mMuted;

    public Sequence getSequence() {
        return mSequence;
    }

    public boolean isNoVideo() {
        return mNoVideo;
    }

    public void setNoVideo(boolean noVideo) {
        mNoVideo = noVideo;
    }

    public MediaPlayerExtension getActivityMediaPlayer() {
        return mActivityMediaPlayer;
    }

    // TODO==010 Move to Pose and get rid of the parameter?

    public void registerLoadedObserver(SequenceLoadedObserver observer) {
        if (mLoadedObserverCollection == null) {
            mLoadedObserverCollection = new ArrayList<>(1);
        }
        mLoadedObserverCollection.add(observer);
    }

    public void unregisterLoadedObserver(SequenceLoadedObserver observer) {
        mLoadedObserverCollection.remove(observer);
    }

    /**
     * Notify only once.
     */
    public void notifyLoadedObservers() {
        Log.d(TAG, "notifyLoadedObservers");
        // https://stackoverflow.com/a/223929 (not used)
        for (SequenceLoadedObserver observer : mLoadedObserverCollection) {
            if (mSequence != null) {
                observer.update();
                unregisterLoadedObserver(observer);
            } else {
                throw new NullPointerException("mSequence was not yet set!");
            }
        }
    }

    /**
     * Load Instructions for the provided Pose.
     * Once done loading the last pose, this starts the sequence through the observer design
     * pattern.
     *
     * @param pose A Pose to load InstructionTemplates and InstructionVariants for.
     */
    public void loadInstructions(Pose pose) {
        TemplateWithVariantsViewModel templateWithVariantsViewModel =
                new TemplateWithVariantsViewModel(getApplication());
        // TODO==020 get the variant based on the user's preferences (skill,
        //  duration, etc)
        templateWithVariantsViewModel.getVariantsOfATemplate(
                pose.getId()).observe(
                FullscreenActivity.this,
                new Observer<List<TemplateWithVariants>>() {
                    public void onChanged(
                            @NonNull List<TemplateWithVariants>
                                    templatesWithVariants) {
                        List<InstructionTemplate> templatesOfPose = new
                                ArrayList<>(templatesWithVariants.size());
                        for (TemplateWithVariants templateWithVariants :
                                templatesWithVariants) {
                            templatesOfPose.add(templateWithVariants.template);
                            // assign all variants to their related template
                            templateWithVariants.template.setInstructionVariants(
                                    templateWithVariants.variants);
                        }
                        InstructionTemplate firstTemplate = templatesOfPose.get(0);
                        // set the first template (depending on order) as active
                        pose.setActiveTemplate(firstTemplate);
                        for (InstructionTemplate instructionTemplate : templatesOfPose) {
                            // TODO==020 can't we just find templates with the
                            //  same parent pose where we call getSiblings?
                            // TODO==099 CHECK: this gets all templates including those not being
                            //  played because pose time is shorter than sum of template
                            //  durations. Is this what we want? I think so, because we want to
                            //  be able to dynamically increase the pose duration on style of
                            //  yoga, i.e. flow vs yin vs power etc.
                            instructionTemplate.setSiblings(templatesOfPose);
                            instructionTemplate.setPose(pose);
                        }

                        // assign the templates to the related pose
                        pose.setInstructionTemplates(templatesOfPose);
                        mPosesSetCount += 1;
                        //
                        //
                        if (mPosesSetCount >= mSequence.getPosesCount()) {
                            for (Pose pose : mSequenceWithPoses.poses) {
                                mContentView.addViewsToAnimator(pose,
                                        FullscreenActivity.this);
                            }
                            Log.d(TAG, "All pose views loaded: " + mContentView);
                            notifyLoadedObservers();
                        }
                    }
                }
        );
    }

    // TODO==020 move to a loading screen view? Separate activity or just a view?

    /**
     * Method to add all views belonging to the selected sequence/routine. I.e. this adds all poses.
     * Once done, get and start the sequence.
     */
    protected void populateViews() {
        Log.d(TAG, "populateViews");
        // Populate all views first,
        if (BuildConfig.DEBUG && mContentView.getChildCount() != 0) {
            for (int i = 0; i < mContentView.getChildCount(); i++) {
                if (!(mContentView.getChildAt(i) instanceof MediaView)) {
                    Log.d(TAG, "Skip populate views other than MediaView");
                    // Uncomment continue on adding more conditions.
//                    continue;
                } else {
                    throw new AssertionError("Expect views to be manually populated only once");
                }
            }
        }
        // TODO==050 Feature: select a sequence
        // Dummy view which cannot be "flinged" to
        // https://stackoverflow.com/questions/8460680
        View dummyView = new View(this);
        mContentView.addView(dummyView);

        SequenceWithPosesViewModel sequenceWithPosesViewModel = new ViewModelProvider(this).get(
                SequenceWithPosesViewModel.class);
        // TODO==55(DEPENDS on 50) remove hard coded sId: 1 ( or 2, 3 ):
        // TODO==DEBUG NOTE: Sequence is set here.
        sequenceWithPosesViewModel.getPosesOfASequence(3).observe(
                this,
                new Observer<SequenceWithPoses>() {
                    @Override
                    public void onChanged(SequenceWithPoses sequenceWithPoses) {
                        mSequenceWithPoses = sequenceWithPoses;
                        mSequence = mSequenceWithPoses.sequence;
                        // Set actionbar title
                        if (mActionbar != null) {
                            mActionbar.setTitle(mSequence.getName());
                        }
                        mSequence.setPoses(sequenceWithPoses.poses);
                        InstructionTemplate.setSequence(mSequence);
                        mPosesCount = mSequenceWithPoses.poses.size();

                        mSequence.setPosesCount(mPosesCount);
                        Log.d(TAG, "getPosesOfASequence Observer onChanged");
                        // TODO==070 what if we add a lot of views?
                        //  https://developer.android.com/training/improving-layouts/optimizing
                        //  -layout.html
                        mSequence.setContentView(mContentView);
                        for (Pose pose : sequenceWithPoses.poses) {
                            pose.setSequence(mSequence);
                            loadInstructions(pose);
                        }
                    }
                }
        );
    }

    // TODO==010 move as much as possible from this to onPostCreate or onStart or onResume
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
        SequenceLoadedObserver sequenceLoadedObserver = new SequenceLoadedObserver(
                this, getLifecycle());
        registerLoadedObserver(sequenceLoadedObserver);

        // TODO==DEBUG set video or image mode here, when going directly to FullscreenActivity:
        setNoVideo(getIntent().getBooleanExtra(MainActivity.EXTRA_NOVIDEO, false));
        setContentView(R.layout.activity_fullscreen);

        mContentView = findViewById(R.id.fullscreen_content);
        mContentView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_VISIBLE | Window.FEATURE_ACTION_BAR_OVERLAY
        );
        mActionbar = getSupportActionBar();
        if (mActionbar != null) {
            mActionbar.setDisplayHomeAsUpEnabled(true);
//            https://stackoverflow.com/a/38427941
            mActionbar.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            mActionbar.setTitle("Loading");
        }
        mVisible = true;

        //TODO==050 use android.app.Activity.getLayoutInflater() or Context.getSystemService
        // instead?
        mContentView.setFullscreenActivity(this);

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            /**
             * Even though this is empty, we need this for gestures to work.
             *
             * @param view The clicked view
             */
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: contentView");
                // Intentionally do nothing here. Removing this would prevent gestures from working.
            }
        });

        // Handle flings
        mDetector = new GestureDetector(this, new MyGestureListener());
        mContentView.setOnTouchListener(new View.OnTouchListener() {
            /**
             * Override required to enable flings.
             *
             * @param v     The touched view
             * @param event the touch event
             * @return onTouchEvent: true if the GestureDetector.OnGestureListener consumed the
             * event, else false.
             */
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // https://stackoverflow.com/a/28551278
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    v.performClick();
                }
                return mDetector.onTouchEvent(event);
            }
        });

        // Call method to add all views belonging to the selected sequence/routine
        populateViews();
    }

    // Upon interacting with UI controls, delay any scheduled hide()
    // operations to prevent the jarring behavior of controls going away
    // while interacting with the UI.
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onPostCreate");
        super.onPostCreate(savedInstanceState);
        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        // TODO==999 feature: disable this for a more peaceful experience
        delayedHide(ACTIONBAR_HIDE_DELAY);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    // TODO==020 Handle onPause and onStop and onRestart

    @Override
    public void onBackPressed() {
        mContentView.cleanup();
        mActivityMediaPlayer.release();
        mActivityMediaPlayer = null;
        mSequence.getHandler().removeCallbacksAndMessages(null);
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            // TODO==999A NOTE this obviously does not work when mainviewactivity redirects to this
            //  fullscreenactivity, but that's intentional to speed up testing.
            mContentView.cleanup();
            mActivityMediaPlayer.release();
            mActivityMediaPlayer = null;
            mSequence.getHandler().removeCallbacksAndMessages(null);
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        if (mActionbar != null) {
            mActionbar.hide();
        }
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        // https://stackoverflow.com/a/28041425
        mContentView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                        View.SYSTEM_UI_FLAG_VISIBLE | Window.FEATURE_ACTION_BAR_OVERLAY
        );
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    /**
     * Removes callbacks, messages, stops and resets MediaPlayer.
     */
    private void resetMediaPlayer() {
        mSequence.getHandler().removeCallbacksAndMessages(null);
        Handler poseHandler = mContentView.getPoseHandler();
        poseHandler.removeCallbacksAndMessages(null);
        MediaPlayer mp = getActivityMediaPlayer();
        if (mp != null) {
            Log.d(TAG, "mp volume 0, stop, reset");
            // TODO==020 tweakable setting delay before play sound after
            if (mp.isPlaying()) {
                mp.stop();
            }
            mp.reset();
        } else {
            Log.e(TAG, "RESET FAILED!");
        }
    }

    public boolean isMuted() {
        return mMuted;
    }

    public void setMuted(boolean muted) {
        mMuted = muted;
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                                float distanceY) {
            return true;
        }

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {
            float deltaX = abs(event1.getX() - event2.getX());
            float deltaY = abs(event1.getY() - event2.getY());
            double size = sqrt((deltaX * deltaX) + (deltaY * deltaY));
            // Require to fling at least 200ish pixels. Not really considering screen size and
            // ppi and all that. Also not considering that we mainly want to take horizontal
            // flings into account, but maybe we want to use vertical ones too. But then we
            // should look at them separately, never the diagonal.
            if (size < 200.0) {
                return true;
            }
            Log.d(TAG, "fling size: " + size);
            // If duration remaining on instructions/view is less than 200ms, don't do the fling
            //  action, except for going back at the end.
            MediaView displayedMediaView =
                    (MediaView) mContentView.getChildAt(mContentView.getDisplayedChild());
            InstructionTemplate instructionTemplate = displayedMediaView.getInstructionTemplate();
            int remainingDuration = instructionTemplate.getRemainingDuration();
            if (remainingDuration != 0 && remainingDuration <= 150) {
                Log.d(TAG, "Flinging in grace period denied.");
                return true;
            }
            resetMediaPlayer();
            // TODO==060 less sensitive?
            if (mSequence.isPaused()) {
                Log.d(TAG, "IS PAUSED");
                // TODO==099 DRY
                displayedMediaView.setAlpha(1.0f);
                mSequence.setPaused(false);
            }
            // TODO==060 probably have to get sensitivity from global user settings?
            Log.d(TAG, "VelX" + velocityX);
            if (velocityX < -2000f) {
                if (mSequence.isSuspended()) {
                    Log.d(TAG, "Already suspended: not flinging forward");
                    return true;
                }
                // TODO==020 can we use mShowNextViewRunnable instead?
                mContentView.showNext();
            } else if (velocityX > 2000f) {
                if (mSequence.isSuspended()) {
                    mSequence.setSuspended(false);
                }
                mContentView.showPrevious();
            }
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Log.d(TAG, "onDoubleTap");
            // Check duration on instructions/view remaining, if less than 200ms,
            //  don't do the doubleTap action.
            MediaView displayedMediaView =
                    (MediaView) mContentView.getChildAt(mContentView.getDisplayedChild());
            InstructionTemplate instructionTemplate = displayedMediaView.getInstructionTemplate();
            if (!mSequence.isPaused() && instructionTemplate.getRemainingDuration() <= 200) {
                return false;
            }
            mContentView.cleanup();
            int whichChild = mContentView.getDisplayedChild();
            // TODO==020 restore for imageView
            if (mSequence.isSuspended()) {
                // TODO==090 (duplicate TODO) go to summary/congratulations screen instead?
                return super.onDoubleTap(e);
            } else if (!mSequence.isPaused()) {
                // TODO==020A fix alpha not working for video, use other method?
                // TODO==020 restore for imageView
//                displayedMediaView.setAlpha(0.8f);
                mSequence.pause();
            } else {
                // TODO==20A fix alpha not working for video, use other method?
                //  Try https://stackoverflow.com/a/20798380
                // TODO==020 restore for imageView
//                displayedMediaView.setAlpha(1.0f);
                mSequence.resumeMediaView(whichChild, mContentView);
            }
            return super.onDoubleTap(e);
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            toggle();
            return super.onSingleTapConfirmed(e);
        }
    }
}
