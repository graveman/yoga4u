/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cgullstar.yoga4u.R;
import com.cgullstar.yoga4u.adapters.WordListAdapter;
import com.cgullstar.yoga4u.entities.Word;
import com.cgullstar.yoga4u.roomdb.WordViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;


public class MainActivity extends AppCompatActivity {
    /**
     * Do not play Instruction audio at all.
     */
    public static final int SILENT_VERBOSITY = -999;
    public static final String EXTRA_NOVIDEO = "com.cgullstar.yoga4u.NOVIDEO";
    public static final String PREFERENCES = "com.cgullstar.yoga4u.PREFERENCES";
    private WordViewModel mWordViewModel;
    public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;
    String TAG = "MainActivity";
    /**
     * Only say and show (todo) the name of the pose (note: mute can be set at any
     * time (todo)).
     */
    public static final int NAME_ONLY = -100;
    /**
     * Only play InstructionTemplates marked as essential.
     */
    public static final int ESSENTIAL_ONLY = -50;
    // Note: positive number verbosity constants are used as a percentage multiplier in
    // InstructionTemplate#start.
    /**
     * Essential instructions plus up to 25% (in duration) more instructions.
     */
    public static final int LOW_VERBOSITY = 25;
    /**
     * Essential instructions plus up to 50% (in duration) more instructions.
     */
    public static final int MEDIUM_VERBOSITY = 50;
    /**
     * Essential instructions plus up to 50% (in duration) more instructions.
     */
    public static final int HIGH_VERBOSITY = 75;
    /**
     * Essential instructions plus all playable instructions.
     */
    public static final int FULL_VERBOSITY = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "Creating MainActivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final WordListAdapter adapter = new WordListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // User preferences
        // https://stackoverflow.com/a/3851585
        SharedPreferences settings = getSharedPreferences(PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = settings.edit();
        prefEditor.putString("userName", "John Doe");
        prefEditor.putInt("userAge", 22);
        prefEditor.putString("gender", "Male");
        prefEditor.putString("skill", "intermediate");
        // TODO==DEBUG Issue #2 Verbosity Levels of Instructions. Name only verbosity
        //  valid options: SILENT_VERBOSITY, NAME_ONLY, ESSENTIAL_ONLY, LOW_VERBOSITY,
        //  MEDIUM_VERBOSITY, HIGH_VERBOSITY, FULL_VERBOSITY
        prefEditor.putInt("verbosity", FULL_VERBOSITY);
        // TODO==999 consider mute persistence, also see FullscreenActivity.mMuted
        prefEditor.putString("focusArea", "none");
        prefEditor.putInt("totalDuration", 20);
        prefEditor.putInt("languageId", 2);
        prefEditor.apply();

        // Observer
        mWordViewModel = new ViewModelProvider(this).get(WordViewModel.class);
        mWordViewModel.getAllWords().observe(this, new Observer<List<Word>>() {
            @Override
            public void onChanged(@Nullable final List<Word> words) {
                // Update the cached copy of the words in the adapter.
                adapter.setWords(words);
            }
        });

        FloatingActionButton fab = findViewById(R.id.fabAdd);
        FloatingActionButton fabDelete = findViewById(R.id.fabDelete);
        FloatingActionButton fabStart = findViewById(R.id.fabStart);
        FloatingActionButton fabStart2 = findViewById(R.id.fabStart2);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewWordActivity.class);
                startActivityForResult(intent, NEW_WORD_ACTIVITY_REQUEST_CODE);
            }
        });

        fabDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWordViewModel.deleteAll();
            }
        });

        fabStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FullscreenActivity.class);
                intent.putExtra(EXTRA_NOVIDEO, false);
                startActivity(intent);
            }
        });

        fabStart2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, FullscreenActivity.class);
                intent.putExtra(EXTRA_NOVIDEO, true);
                startActivity(intent);
            }
        });

        Log.d(TAG, "Finished creating main activity");
        // TODO==DEBUG go to fullscreenactivity immediately for testing
        Intent intent = new Intent(MainActivity.this, FullscreenActivity.class);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            Word word = new Word(data.getStringExtra(NewWordActivity.EXTRA_REPLY));
            mWordViewModel.insert(word);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }
}
