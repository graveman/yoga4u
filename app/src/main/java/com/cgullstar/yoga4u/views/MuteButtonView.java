/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.views;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.util.Log;

import androidx.appcompat.widget.AppCompatTextView;

import com.cgullstar.yoga4u.activities.FullscreenActivity;

/**
 * Override for accessibility.
 * https://stackoverflow.com/a/50345118
 * This is a Button, but we have to extend it to facilitate accessibility it seems.
 * Features: mute or unmute the MediaPlayer of the FullscreenActivity on click.
 */
public class MuteButtonView extends AppCompatTextView {
    String TAG = "MuteButtonView";

    public MuteButtonView(Context context) {
        super(context);
    }

    public MuteButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean performClick() {
        // Note: I had an override on onTouchEvent here too, but flipMuteMedia ended up being
        // called twice.
        flipMuteMedia();
        return super.performClick();
    }

    /**
     * Mute or unmute the MediaPlayer of the FullscreenActivity.
     */
    private void flipMuteMedia() {
        FullscreenActivity fullscreenActivity = (FullscreenActivity) this.getContext();
        MediaPlayer mediaPlayer = fullscreenActivity.getActivityMediaPlayer();
        boolean isMuted = fullscreenActivity.isMuted();
        isMuted = !isMuted;
        Log.d(TAG, "onTouch: muted: " + isMuted);
        fullscreenActivity.setMuted(isMuted);
        if (mediaPlayer != null) {
            if (isMuted) {
                mediaPlayer.setVolume(0f, 0f);
            } else {
                mediaPlayer.setVolume(1f, 1f);
            }
        }
    }
}
