/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.cgullstar.yoga4u.entities.InstructionVariant;

import java.util.List;

@Dao
public interface InstructionVariantDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertInstructionVariants(InstructionVariant... instructionVariants);

    /**
     * Get all InstructionVariants belonging to a Pose if the related InstructionTemplate is
     * related to the Pose as well (or just see the join).
     *
     * @param poseId Pose to which the variant should belong (through an
     *               {@link com.cgullstar.yoga4u.entities.InstructionTemplate}).
     * @return a livedata list of InstructionVariants related to the passed Pose.
     */

    @Query("SELECT v.* FROM instruction_variant AS v JOIN instruction_template AS t " +
            "ON v.instruction_template_id = t.id WHERE t.pose_id = :poseId ORDER BY id")
    LiveData<List<InstructionVariant>> getInstructionVariant(int poseId);
}
