/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.interfaces;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.cgullstar.yoga4u.entities.Pose;
import com.cgullstar.yoga4u.entities.Sequence;
import com.cgullstar.yoga4u.entities.SequencePoseXRef;
import com.cgullstar.yoga4u.entities.relations.SequenceWithPoses;

import java.util.List;

@Dao
public interface SequencePoseDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertSequence(Sequence sequence);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertSequences(Sequence... sequences);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertXRefs(SequencePoseXRef... xRefs);

    // TODO==999 any way to assert singleton here? don't want to LIMIT 1
    @Query("SELECT * FROM sequence WHERE id = :sId")
    LiveData<Sequence> getSequenceById(int sId);

    @Query("SELECT * FROM sequence WHERE name = :name")
    LiveData<Sequence> getSequenceByName(String name);

    @Transaction
    @Query("SELECT sid, pid, ordering FROM sequenceposexref WHERE sid = :sId ORDER " +
            "BY ordering")
    LiveData<List<SequencePoseXRef>> getOneSequencesPoses(int sId);

    @Query("SELECT * FROM sequence")
    LiveData<List<Sequence>> getSequenceWithPoses();

    @Transaction
    @Query("SELECT seq.id, seq.name FROM sequence AS seq INNER JOIN sequenceposexref AS ref " +
            "ON seq.id = ref.sid WHERE id = :sId ORDER BY ref.ordering")
    LiveData<SequenceWithPoses> getPosesOfASequence(int sId);

    @Query("SELECT pose.* FROM pose INNER JOIN " +
            "sequenceposexref ref " +
            "WHERE " +
            "sid = :sId AND ordering = " +
            ":cId")
    LiveData<Pose> getPoseByChild(int sId, int cId);
}
