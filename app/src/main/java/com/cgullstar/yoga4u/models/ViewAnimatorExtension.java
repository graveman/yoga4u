/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.models;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ViewAnimator;

import com.cgullstar.yoga4u.activities.FullscreenActivity;
import com.cgullstar.yoga4u.entities.InstructionTemplate;
import com.cgullstar.yoga4u.entities.InstructionVariant;
import com.cgullstar.yoga4u.entities.Pose;
import com.cgullstar.yoga4u.entities.Sequence;
import com.cgullstar.yoga4u.interfaces.MediaView;

import java.util.List;
import java.util.MissingResourceException;


public class ViewAnimatorExtension extends ViewAnimator {
    String TAG = "ViewAnimatorExtension";
    private Handler mPoseHandler = new Handler();
    private Sequence mSequence;
    private FullscreenActivity mFullscreenActivity;
    final Runnable cleanupRunnable = () -> {
        if (!cleanup()) {
            Log.d(TAG, "cleanup false");
        } else {
            Log.d(TAG, "cleanup true");
        }
    };

    final Runnable suspendRunnable = () -> {
        mSequence.suspend();
    };

    private Runnable startNextInstructionsRunnable = () -> startTemplate("next");
    private Runnable startPreviousInstructionsRunnable = () -> startTemplate("previous");
    private int mMediaViewCount;
    private MediaView mFirstMediaView;

    /**
     * Starts either the next or previous template, as specified. When trying to get the previous
     * template when already on the first template, the first template will be restarted. When
     * trying to get the next template when already on the last template, the
     * {@link InstructionTemplate#start(Context)} method will call
     * {@link Sequence#suspend()} to pause the session at the last template.
     * TODO==999 go to summary screen instead of pausing there.
     *
     * @param nextOrPrevious Specify whether to start the next or previous template.
     */
    private void startTemplate(String nextOrPrevious) {
        Log.d(TAG, "#startTemplate");
        MediaView mediaView;
        if (mFirstMediaView == null) {
            setFirstMediaView();
        }
        int indexOfFirstMediaView = indexOfChild((View) mFirstMediaView);
        if (nextOrPrevious == "previous" && getDisplayedChild() <= indexOfFirstMediaView) {
            // TODO==999 somewhere else as well, but think about going back to the "main template"
            //  (keyword, search for that) part of the pose, instead of how to get into it?
            mFirstMediaView.getInstructionTemplate().start(getContext());
        } else if (nextOrPrevious == "previous") {
            Log.d(TAG, "startTemplate PREVIOUS");
            mediaView = (MediaView) getChildAt(getDisplayedChild() - 1);
            Pose previousPose = mediaView.getPose();
            InstructionTemplate firstTemplate = previousPose.getFirstTemplate();
            firstTemplate.start(getContext());
        } else if (nextOrPrevious == "next") {
            Log.d(TAG, "startTemplate NEXT");
            mediaView = (MediaView) getChildAt(getDisplayedChild());
            mediaView.getInstructionTemplate().getNext().start(getContext());
        } else {
            throw new IllegalStateException("startTemplate must be called with previous or next.");
        }
    }

    public ViewAnimatorExtension(Context context) {
        super(context);
    }

    public ViewAnimatorExtension(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FullscreenActivity getFullscreenActivity() {
        return mFullscreenActivity;
    }

    public void setFullscreenActivity(FullscreenActivity fullscreenActivity) {
        mFullscreenActivity = fullscreenActivity;
    }

    public void setSequence(Sequence sequence) {
        mSequence = sequence;
    }

    public boolean cleanup() {
        // This doesn't do much anymore!
        // On pause it should not reset any mediaPlayer (we want to resume audio playback).
        Log.d(TAG, "cleanup");
        mSequence.getHandler().removeCallbacksAndMessages(null);
        Log.d(TAG, "cleanup removecallbacks");
        // Case no content views (only the dummy view):
        return getChildCount() > 1;
    }

    /**
     * Override for accessibility
     *
     * @return super
     */
    @Override
    public boolean performClick() {
        return super.performClick();
    }

    /**
     * Shows next, or pauses playback if there is no next.
     */
    @Override
    public void showNext() {
        // TODO==020 make clear what the (single) responsibilities are of sequenceHandler and
        //  poseHandler (mPoseHandler).
        Log.d(TAG, "showNext");
        Handler sequenceHandler = mSequence.getHandler();
        Log.d(TAG, "cleanupRunnable");
        sequenceHandler.post(cleanupRunnable);
        int whichChild = getDisplayedChild();
        // We need pause here next to InstructionTemplate#start() because they will also play
        // audio/video. When flinging to the next template, we do not want that.
        if (whichChild >= getChildCount() - 1) {
            mPoseHandler.post(suspendRunnable);
        } else {
            // TODO==030 add a few seconds of pause after flinging.
            // TODO==100 [FEATURE] go to "mSequence complete" view/activity
            // TODO==20A Find cases where we want to remove this callback
            mPoseHandler.post(startNextInstructionsRunnable);
        }
    }

    @Override
    public void showPrevious() {
        Log.d(TAG, "showPrevious");
        Handler sequenceHandler = mSequence.getHandler();
        Log.d(TAG, "cleanupRunnable");
        if (!sequenceHandler.post(cleanupRunnable)) {
            Log.d(TAG, "oops5820391");
        }
        int whichChild = getDisplayedChild();
        MediaView prevMediaView;
        // Check if the previous child is a MediaView
        if (!(getChildAt(whichChild - 1) instanceof MediaView)) {
            // Show the first Instruction view
            prevMediaView = getFirstMediaView();
        } else {
            prevMediaView = (MediaView) getChildAt(whichChild - 1);
        }
        Pose previousPose = prevMediaView.getPose();
        // As we start from the first Instruction, reset the duration of instructions for
        // the target pose played.
        // (See InstructionTemplate TODO==998A: Have to adjust this when we jump to an
        //  Instruction which is not the first)
        previousPose.setInstructionsDurationCumulative(0);
        InstructionTemplate firstTemplate = previousPose.getFirstTemplate();
        previousPose.setActiveTemplate(firstTemplate);
        if (previousPose.getActiveTmpl() == null) {
            throw new IllegalStateException("template not set properly");
        }
        // TODO==20A Find cases where we want to remove this callback
        mPoseHandler.post(startPreviousInstructionsRunnable);
    }

    public MediaView getFirstMediaView() {
        if (mFirstMediaView == null) {
            setFirstMediaView();
        }
        return mFirstMediaView;
    }

    private void setFirstMediaView() {
        for (int i = 0; i < getChildCount(); i++) {
            if (getChildAt(i) instanceof MediaView) {
                mFirstMediaView = (MediaView) getChildAt(i);
                return;
            }
        }
    }

    /**
     * Adds views (image or video) to the sequence (ViewAnimator), and:
     * associates an InstructionVariant with each of the Pose's InstructionTemplates.
     * TODO==999 ensure addViewsToAnimator (or at least setInstructionVariant) is called before any
     * {@link InstructionTemplate#getInstructionVariant()}.
     *
     * @param pose    Pose to add views for and associate InstructionVariants with.
     * @param context activity context
     */
    public void addViewsToAnimator(Pose pose, FullscreenActivity context) {
        // TODO==999 add fallback on image, fallback on text
        // TODO==099 this should really just say pose.getInstructions
        List<InstructionTemplate> templates = pose.getInstructionTemplates(mFullscreenActivity);
        // Associate the correct variant with the template, according to user preferences.
        for (InstructionTemplate template : templates) {
            // Trim siblings to siblings to be played.
            template.setSiblings(templates);
            InstructionVariant priorityVariant = null;
            priorityVariant = template.getPriorityVariant(context);
            if (priorityVariant == null) {
                throw new MissingResourceException(
                        "No InstructionVariant with default language en_us found!",
                        "InstructionVariant", "languageId==1");
            }
            template.setInstructionVariant(priorityVariant);
            if (context.isNoVideo()) {
                ImageViewExtension imageView = new ImageViewExtension(
                        context.getApplicationContext());
                int imageId = priorityVariant.getImageId();
                imageView.setImageResource(imageId);
                imageView.setPose(pose);
                // TODO==050 use mediaView instead?
                imageView.setInstructionTemplate(template);
                Log.d(TAG, "add image view for pose: " + pose.getName());
                addView(imageView);
                Log.d(TAG, "childcount: " + getChildCount());
            } else {
                VideoViewExtension videoView = new VideoViewExtension(
                        context.getApplicationContext());
                // TODO==099 check if video exists
                Uri uri = Uri.parse("android.resource://" + context.getPackageName() +
                        "/" + priorityVariant.getVideoId());
                videoView.setVideoURI(uri);
                videoView.setPose(pose);
                videoView.setInstructionTemplate(template);
                template.setVideoView(videoView);

                videoView.addOnLayoutChangeListener((v, left, top, right, bottom, oldLeft, oldTop
                        , oldRight, oldBottom) -> {
                    Log.d(TAG, "Centering the VideoView");
                    float videoWidth = videoView.getWidth();
                    float videoHeight = videoView.getHeight();
                    videoView.setX(getWidth() / 2f - videoWidth / 2);
                    videoView.setY(getHeight() / 2f - videoHeight / 2);
                });

                Log.d(TAG, "add video view for pose: " + pose.getName());
                addView(videoView);
            }
        }
    }

    private void setMediaViewCount() {
        int count = 0;
        for (int i = 0; i < getChildCount(); i++) {
            if (getChildAt(i) instanceof MediaView) {
                count++;
            }
        }
        mMediaViewCount = count;
    }

    /**
     * @return number of image or video views associated with this {@link ViewAnimatorExtension}.
     */
    public int getMediaViewCount() {
        if (mMediaViewCount == 0) {
            setMediaViewCount();
        }
        return mMediaViewCount;
    }

    public Handler getPoseHandler() {
        return mPoseHandler;
    }

    public int getIndexByTemplate(InstructionTemplate instructionTemplate) {
        for (int i = 0; i < getChildCount(); i++) {
            // Check for dummy view or buttons etc.
            if (getChildAt(i) instanceof MediaView) {
                MediaView mediaView = (MediaView) getChildAt(i);
                if (mediaView.getInstructionTemplate() == instructionTemplate) {
                    return i;
                }
            }
        }
        throw new IllegalStateException("Could not find a View to match the given " +
                "InstructionTemplate.");
    }
}
