/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.models;

import android.content.Context;

import androidx.appcompat.widget.AppCompatImageView;

import com.cgullstar.yoga4u.entities.InstructionTemplate;
import com.cgullstar.yoga4u.entities.Pose;
import com.cgullstar.yoga4u.interfaces.MediaView;

public class ImageViewExtension extends AppCompatImageView implements MediaView {
    private Pose mPose;
    private InstructionTemplate mInstructionTemplate;

    public ImageViewExtension(Context context) {
        super(context);
    }

    public Pose getPose() {
        return mPose;
    }

    public InstructionTemplate getInstructionTemplate() {
        return mInstructionTemplate;
    }

    public void setPose(Pose pose) {
        mPose = pose;
    }

    public void setInstructionTemplate(InstructionTemplate instructionTemplate) {
        mInstructionTemplate = instructionTemplate;
    }

    public void stop() {
    }

    /**
     * Well, the image can't really pause, but the related MediaPlayer can...
     *
     * @return
     */
    public boolean canPause() {
        return true;
    }
}
