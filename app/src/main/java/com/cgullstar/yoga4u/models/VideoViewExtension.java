/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.models;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

import com.cgullstar.yoga4u.entities.InstructionTemplate;
import com.cgullstar.yoga4u.entities.Pose;
import com.cgullstar.yoga4u.interfaces.MediaView;

public class VideoViewExtension extends VideoView implements MediaView {
    String TAG = "VideoViewExtension";
    private MediaController mMediaController;

    private Uri mUri;
    private Pose mPose;
    private InstructionTemplate mInstructionTemplate;

    public VideoViewExtension(Context context) {
        super(context);
    }

    public Pose getPose() {
        return mPose;
    }

    public void setPose(Pose pose) {
        mPose = pose;
    }

    public InstructionTemplate getInstructionTemplate() {
        return mInstructionTemplate;
    }

    @Override
    public void setVideoURI(Uri uri) {
        super.setVideoURI(uri);
        mUri = uri;
    }

    public Uri getUri() {
        return mUri;
    }

    public void setInstructionTemplate(InstructionTemplate instructionTemplate) {
        mInstructionTemplate = instructionTemplate;
    }

    public MediaController getMediaController() {
        return mMediaController;
    }

    @Override
    public void setMediaController(MediaController controller) {
        super.setMediaController(controller);
        mMediaController = controller;
    }

    public void stop() {
        InstructionTemplate instructionTemplate = getInstructionTemplate();
        MediaPlayer mediaPlayer = instructionTemplate.getMediaPlayer();
        if (mediaPlayer != null) {
            // NOTE: Putting this in cleanup gives a IllegalStateException.
            if (mediaPlayer.isPlaying()) {
                Log.d(TAG, "mediaPlayer#stopPlayback()");
                stopPlayback();
            }
            Log.d(TAG, "cleaning up mediaPlayer. RELEASE NULL");
            mediaPlayer.release();
            mediaPlayer = null;
            instructionTemplate.setMediaPlayer(null);
        }
    }
}
