/*
 * Copyright 2019 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

//package com.cgullstar.yoga4u.roomdb;

/**
 * Holds all of the data needed for the UI
 */
//public class PoseViewModel extends AndroidViewModel {
//
//    private PoseRepository mRepository;
//    private LiveData<List<Pose>> mAllPoses;
//
//    public PoseViewModel (@NonNull Application application) {
//        super(application);
//        mRepository = new PoseRepository(application);
//        mAllPoses = mRepository.getAllPoses();
//    }
//
//    LiveData<List<Pose>> getAllPoses() { return mAllPoses; }
//
//    public void insert(Pose pose) { mRepository.insert(pose); }
//
//    public void deleteAll() { mRepository.deleteAll(); }
//}
