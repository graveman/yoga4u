/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.roomdb;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.cgullstar.yoga4u.entities.Pose;
import com.cgullstar.yoga4u.entities.Sequence;
import com.cgullstar.yoga4u.entities.SequencePoseXRef;
import com.cgullstar.yoga4u.entities.relations.SequenceWithPoses;
import com.cgullstar.yoga4u.interfaces.SequencePoseDao;

import java.util.ArrayList;
import java.util.List;

class SequenceWithPosesRepository {
    private SequencePoseDao mSequencePoseDao;
    private LiveData<List<SequencePoseXRef>> mOneSequencesPoses;
    private LiveData<SequenceWithPoses> mSequenceWithPoses;
    private LiveData<Pose> mPose;

    // TODO==090 think of a good name for this class
    SequenceWithPosesRepository(Application application) {
        YogaRoomDatabase db = YogaRoomDatabase.getDatabase(application);
        mSequencePoseDao = db.sequencePoseDao();
    }

    LiveData<List<SequencePoseXRef>> getOneSequencesPoses(int sId) {
        mOneSequencesPoses = mSequencePoseDao.getOneSequencesPoses(sId);
        return mOneSequencesPoses;
    }

    LiveData<SequenceWithPoses> getPosesOfASequence(int sId) {
        mSequenceWithPoses = mSequencePoseDao.getPosesOfASequence(sId);
        return mSequenceWithPoses;
    }

    /**
     * @param sId sequence id number
     * @param cId Numerical position (order) in the sequence. Counting starts at 1.
     * @return mPose
     */
    // TODO==030 try to pass objects instead of ids
    LiveData<Pose> getPoseByChild(int sId, int cId) {
        mPose = mSequencePoseDao.getPoseByChild(sId, cId);
        return mPose;
    }

    void composeSequence(Sequence sequence, List<Pose> poses) {
        List<SequencePoseXRef> xRefs = new ArrayList<>();
        for (int i = 0; i < poses.size(); i++) {
            // poses don't have ids yet when initializing the database unless manually assigned.
            SequencePoseXRef xRef = new SequencePoseXRef(sequence.getId(), poses.get(i).getId(), i + 1);
            xRefs.add(xRef);
        }
        // https://stackoverflow.com/a/2843371
        SequencePoseXRef[] simpleArray = new SequencePoseXRef[xRefs.size()];
        xRefs.toArray(simpleArray);
        mSequencePoseDao.insertXRefs(simpleArray);
    }

    /**
     * @param sId Sequence Id
     * @return A Sequence with id == sId
     */
    LiveData<Sequence> getSequence(int sId) {
        return mSequencePoseDao.getSequenceById(sId);
    }
}
