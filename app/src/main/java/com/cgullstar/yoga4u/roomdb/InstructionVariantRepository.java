/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.roomdb;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.cgullstar.yoga4u.entities.InstructionVariant;
import com.cgullstar.yoga4u.interfaces.InstructionVariantDao;

import java.util.List;

class InstructionVariantRepository {
    private InstructionVariantDao mInstructionVariantDao;
    private LiveData<List<InstructionVariant>> mInstructionVariant;

    public InstructionVariantRepository(Context context) {
        YogaRoomDatabase db = YogaRoomDatabase.getDatabase(context);
        mInstructionVariantDao = db.instructionVariantDao();
    }

    public LiveData<List<InstructionVariant>> getInstructionVariant(int poseId) {
        mInstructionVariant = mInstructionVariantDao.getInstructionVariant(poseId);
        return mInstructionVariant;
    }
}
