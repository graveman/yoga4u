/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.roomdb;


//public class PoseRepository {
//    private PoseDao mPoseDao;
//    private LiveData<List<Pose>> mAllPoses;
//
//    // Note that in order to unit test the PoseRepository, you have to remove the Application
//    // dependency. This adds complexity and much more code, and this sample is not about testing.
//    // See the BasicSample in the android-architecture-components repository at
//    // https://github.com/googlesamples
//    PoseRepository(Application application) {
//        YogaRoomDatabase db = YogaRoomDatabase.getDatabase(application);
//        mPoseDao = db.poseDao();
//        mAllPoses = mPoseDao.getAlphabetizedPoses();
//    }
//
//    // Room executes all queries on a separate thread.
//    // Observed LiveData will notify the observer when the data has changed.
//    LiveData<List<Pose>> getAllPoses() {
//        return mAllPoses;
//    }
//
//    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
//    // that you're not doing any long running operations on the main thread, blocking the UI.
//    void insert(Pose pose) {
//        YogaRoomDatabase.databaseWriteExecutor.execute(() -> {
//            mPoseDao.insert(pose);
//        });
//    }
//
//    void deleteAll() {
//        YogaRoomDatabase.databaseWriteExecutor.execute(() -> {
//            mPoseDao.deleteAllPoses();
//        });
//    }
//}
