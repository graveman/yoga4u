/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.roomdb;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.cgullstar.yoga4u.entities.Pose;
import com.cgullstar.yoga4u.entities.Sequence;
import com.cgullstar.yoga4u.entities.relations.SequenceWithPoses;

public class SequenceWithPosesViewModel extends AndroidViewModel {
    private SequenceWithPosesRepository mRepository;
    private LiveData<SequenceWithPoses> mPosesOfASequence;
    private LiveData<Pose> mPose;
    public SequenceWithPosesViewModel(@NonNull Application application) {
        super(application);
        mRepository = new SequenceWithPosesRepository(application);
    }

    /**
     * Note: Does not include the dummy view.
     *
     * @param sId A sequence id
     * @return A livedata list of poses
     */
    public LiveData<SequenceWithPoses> getPosesOfASequence(int sId) {
        mPosesOfASequence = mRepository.getPosesOfASequence(sId);
        return mPosesOfASequence;
    }

    /**
     * @param sId A sequence id
     * @return A LiveData Sequence instance
     */
    public LiveData<Sequence> getSequence(int sId) {
        LiveData<Sequence> sequence = mRepository.getSequence(sId);
        return sequence;
    }
}
