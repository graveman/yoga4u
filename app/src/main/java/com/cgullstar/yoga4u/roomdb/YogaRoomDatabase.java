/*
 * Copyright 2019-2020 Kevin Graveman.
 * This file is part of Yoga4u.
 *
 *     Yoga4u is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Yoga4u is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Yoga4u.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.cgullstar.yoga4u.roomdb;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.cgullstar.yoga4u.R;
import com.cgullstar.yoga4u.entities.InstructionTemplate;
import com.cgullstar.yoga4u.entities.InstructionTemplateBuilder;
import com.cgullstar.yoga4u.entities.InstructionVariant;
import com.cgullstar.yoga4u.entities.Language;
import com.cgullstar.yoga4u.entities.Pose;
import com.cgullstar.yoga4u.entities.Sequence;
import com.cgullstar.yoga4u.entities.SequencePoseXRef;
import com.cgullstar.yoga4u.entities.Word;
import com.cgullstar.yoga4u.interfaces.InstructionTemplateDao;
import com.cgullstar.yoga4u.interfaces.InstructionVariantDao;
import com.cgullstar.yoga4u.interfaces.LanguageDao;
import com.cgullstar.yoga4u.interfaces.PoseDao;
import com.cgullstar.yoga4u.interfaces.SequencePoseDao;
import com.cgullstar.yoga4u.interfaces.TemplateVariantDao;
import com.cgullstar.yoga4u.interfaces.WordDao;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Word.class, Pose.class, Sequence.class,
        InstructionTemplate.class, InstructionVariant.class, SequencePoseXRef.class, Language.class}, version = 1,
        exportSchema = false)
abstract class YogaRoomDatabase extends RoomDatabase {
    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private static volatile YogaRoomDatabase INSTANCE;

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        private SequenceWithPosesRepository mSeqWPoseRepository;
        private InstructionTemplateRepository mInstructionTemplateRepository;

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            // TODO==200 https://developer.android.com/training/data-storage/room/prepopulate
            databaseWriteExecutor.execute(() -> {
                // Populate the database in the background.
                LanguageDao language_dao = INSTANCE.languageDao();
                WordDao word_dao = INSTANCE.wordDao();
                PoseDao pose_dao = INSTANCE.poseDao();
                SequencePoseDao sequencePose_dao = INSTANCE.sequencePoseDao();
                TemplateVariantDao templateVariant_dao = INSTANCE.templateVariantDao();
                InstructionTemplateDao instruction_dao = INSTANCE.instructionTemplateDao();
                InstructionVariantDao instruction_var_dao = INSTANCE.instructionVariantDao();
                // TODO==099 [REM] phase out "Word as a room" tutorial content. Including
                //  Word* files.
                Word word = new Word("Down Dog");
                word_dao.insert(word);
                word = new Word("Mountain Pose");
                word_dao.insert(word);

                // Language
                Language en_us = new Language(1, "English (United States)", "en_us");
                Language nl_nl = new Language(2, "Dutch (The Netherlands)", "nl_nl");
                language_dao.insertLanguages(en_us, nl_nl);

                // Create sequences
                Sequence shortSequence = new Sequence(1, "Short Sequence");
                Sequence longSequence = new Sequence(2, "Long Sequence");
                Sequence anneSequence = new Sequence(3, "Anne's Sequence");
                sequencePose_dao.insertSequences(shortSequence, longSequence, anneSequence);

                // autogenerate isn't on time when these poses are used for composing the default
                // sequences so ids are manually assigned here:
                // Create poses
                Pose crossLeggedPose = new Pose(1, "Cross-legged Pose", 8500);
                Pose trianglePose = new Pose(2, "Triangle Pose", 6000);
                Pose fireLogPose = new Pose(3, "Fire Log Pose", 3000);
                Pose boundAnglePose = new Pose(4, "Bound Angle Pose", 1000);
                Pose sillyParentTentPose = new Pose(5, "Silly Parent Tent Pose", 5000);

                // Anne's Poses
                Pose mountainPose = new Pose(6, "Mountain Pose", 10000);
                Pose trianglePose2 = new Pose(7, "Triangle Pose 2", 10000);
                Pose warrior2Pose = new Pose(8, "Warrior 2", 10000);
                Pose extendedSideAnglePose = new Pose(9, "Extended Side Angle Pose", 10000);
                Pose twistedLowLungePose = new Pose(10, "Twisted Low Lunge", 10000);
                Pose plankPose = new Pose(11, "Plank", 10000);
                Pose downDogPose = new Pose(12, "Down Dog Pose", 10000);
                Pose chaturangaPose = new Pose(13, "Chaturanga", 10000);
                Pose upDogPose = new Pose(14, "Up Dog Pose", 10000);
                Pose downDogPose2 = new Pose(15, "Down Dog Pose 2", 10000);
                Pose walkToTopPose = new Pose(16, "Walk To Top", 10000);
                Pose rollUpPose = new Pose(17, "Roll Up", 10000);
                Pose mountainPose2 = new Pose(18, "Mountain Pose 2", 10000);

                pose_dao.insertPoses(crossLeggedPose, trianglePose, fireLogPose, boundAnglePose,
                        sillyParentTentPose);

                // Insert Anne's Poses
                pose_dao.insertPoses(mountainPose, trianglePose2, warrior2Pose,
                        extendedSideAnglePose, twistedLowLungePose, plankPose, downDogPose,
                        chaturangaPose, upDogPose, downDogPose2, walkToTopPose, rollUpPose,
                        mountainPose2);

                // TODO==999 code smell? instantiate Application, or move to an activity are
                //  alternatives.
                Application application = INSTANCE.getApplication();

                // Instructions CANNOT be reused for a pose. TODO==015 implement pose_template_rel
                mInstructionTemplateRepository = new InstructionTemplateRepository(application);

                //TODO==010 why do both pose and template have a name? probably useful but.. why?
                // Well, because you might name the Pose "Down Dog", but the Instructions would
                // be: "Stand up out of Down Dog" as part of a transition. The InstructionVariant
                // then further specifies in instruction_text that you, for example, have to
                // "Gently walk your feet up to your hands, bend your knees, tighten your core,
                // and roll your spine up vertebra by vertebra while gently breathing out." (
                // DISCLAIMER: I'm not a yoga instructor; this is just an example, please consult
                // your yoga instructor professional for all your yoga instructions.).
                //TODO==050 instructionVariant should be gotten according to user preferences
                // Added some instructions marked as announce_pose to test #2: part 1: name only.

                // TODO==000 do not hardcode ids

                InstructionTemplate crossLeggedInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                1, "cross legged instructions", 1, true,
                                0, 1000).setAnnouncePose(true).build();
                InstructionTemplate crossLeggedInstructions2 = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                6, "cross legged instructions part 2", 1, false,
                                1, 5000).build();
                InstructionTemplate crossLeggedInstructions3 = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                7, "cross legged instructions part 2b", 1, false,
                                2, 1100).build();
                InstructionTemplate crossLeggedInstructions4 = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                8, "cross legged instructions part 3", 1, true,
                                3, 1050).build();
                // TESTING Following not loaded because they don't fit in duration of Pose (8500)
                InstructionTemplate crossLeggedInstructions5 = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                9, "cross legged instructions part 3b", 1, false,
                                4, 1000).build();
                InstructionTemplate crossLeggedInstructions6 = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                10, "cross legged instructions part 4", 1, true,
                                5, 1000).build();
                InstructionTemplate crossLeggedInstructions7 = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                11, "cross legged instructions part 5", 1, true,
                                6, 9000).build();

                // TODO==010 add another template with announcePose to an existing Pose.
                InstructionTemplate triangleInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                2, "Triangle Pose", 2, true,
                                0, 6000).setAnnouncePose(true).build();
                InstructionTemplate fireLogInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                3, "Fire Log Pose", 3, true,
                                0, 3000).setAnnouncePose(true).build();
                InstructionTemplate boundAngleInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                4, "Bound Angle Pose", 4, true,
                                0, 1000).setAnnouncePose(true).build();
                InstructionTemplate sillyParentTentInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                5, "Silly Parent Tent Pose", 5, true,
                                0, 5000).setAnnouncePose(true).build();


                // Anne's Instructions
                InstructionTemplate mountainInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                12, "Mountain Pose", 6, true,
                                0, 10000).setAnnouncePose(true).build();
                // TODO WIP reuse?
                InstructionTemplate triangleInstructions2 = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                13, "Triangle Pose 2", 7, true,
                                1, 10000).setAnnouncePose(true).build();
                InstructionTemplate warrior2Instructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                14, "Warrior 2", 8, true,
                                2, 10000).setAnnouncePose(true).build();
                InstructionTemplate extendedSideAngleInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                15, "Extended Side Angle Pose", 9, true,
                                3, 10000).setAnnouncePose(true).build();
                InstructionTemplate twistedLowLungeInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                16, "Twisted Low Lunge", 10, true,
                                4, 10000).setAnnouncePose(true).build();
                InstructionTemplate plankInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                17, "Plank", 11, true,
                                5, 10000).setAnnouncePose(true).build();
                InstructionTemplate downDogInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                18, "Down Dog Pose", 12, true,
                                6, 10000).setAnnouncePose(true).build();
                InstructionTemplate chaturangaInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                19, "Chaturanga", 13, true,
                                7, 10000).setAnnouncePose(true).build();
                InstructionTemplate upDogInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                20, "Up Dog Pose", 14, true,
                                8, 10000).setAnnouncePose(true).build();
                // TODO WIP reuse?
                InstructionTemplate downDogInstructions2 = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                21, "Down Dog Pose 2", 15, true,
                                9, 10000).setAnnouncePose(true).build();
                InstructionTemplate walkToTopInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                22, "Walk To Top", 16, true,
                                10, 10000).setAnnouncePose(true).build();
                InstructionTemplate rollUpInstructions = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                23, "Roll Up", 17, true,
                                11, 10000).setAnnouncePose(true).build();
                // TODO WIP is it possible to reuse Instructions?
                InstructionTemplate mountainInstructions2 = new InstructionTemplateBuilder()
                        .setRequiredFields(
                                24, "Mountain Pose 2", 18, true,
                                12, 10000).setAnnouncePose(true).build();

                instruction_dao.insertInstructionTemplates(crossLeggedInstructions,
                        crossLeggedInstructions2, crossLeggedInstructions3,
                        crossLeggedInstructions4, crossLeggedInstructions5,
                        crossLeggedInstructions6, crossLeggedInstructions7, triangleInstructions,
                        fireLogInstructions,
                        boundAngleInstructions, sillyParentTentInstructions);

                // Insert Anne's Instructions
                instruction_dao.insertInstructionTemplates(mountainInstructions,
                        triangleInstructions2, warrior2Instructions, extendedSideAngleInstructions,
                        twistedLowLungeInstructions, plankInstructions, downDogInstructions,
                        chaturangaInstructions, upDogInstructions, downDogInstructions2,
                        walkToTopInstructions, rollUpInstructions, mountainInstructions2
                );

                // TODO==020 can we programmatically generate variants for templates which do not
                //  have variants yet based on some defaults?
                InstructionVariant crossLeggedInstructionVariant = new InstructionVariant(1,
                        "crosslegged01", 1,
                        R.raw.risset_drum01, R.drawable.crosslegged01,
                        R.raw.boat_pose_paripurna_navasana, 1);
                InstructionVariant crossLeggedInstructionVariant2 = new InstructionVariant(6,
                        "crosslegged variant part 2", 6,
                        R.raw.risset_drum01, R.drawable.yogamountain01,
                        R.raw.child_pose_balasana_yoga_oefeningen,
                        1);
                InstructionVariant crossLeggedInstruction2Variant2 = new InstructionVariant(7,
                        "crosslegged SECOND variant (e.g. other language) part 2", 6,
                        R.raw.risset_drum01, R.drawable.boysuperherocrosslegged,
                        R.raw.crane_pose_bakasana_, 2);

                InstructionVariant crossLeggedInstructionVariant3 = new InstructionVariant(8,
                        "crosslegged part 3", 7,
                        R.raw.risset_drum01, R.drawable.cartooncrosslegged,
                        R.raw.pigeon2, 1);
                InstructionVariant crossLeggedInstructionVariant4 = new InstructionVariant(9,
                        "crosslegged part 4", 8,
                        R.raw.risset_drum01, R.drawable.longhaircrosslegged,
                        R.raw.pigeon2, 1);
                // TESTING Following not loaded because they don't fit in duration of Pose (8500)
                InstructionVariant crossLeggedInstructionVariant5 = new InstructionVariant(10,
                        "crosslegged part 5", 9,
                        R.raw.risset_drum01, R.drawable.rollingmat,
                        R.raw.pigeon2, 1);
                InstructionVariant crossLeggedInstructionVariant6 = new InstructionVariant(11,
                        "crosslegged part 6", 10,
                        R.raw.risset_drum01, R.drawable.sidestand,
                        R.raw.pigeon2, 1);
                InstructionVariant crossLeggedInstructionVariant7 = new InstructionVariant(12,
                        "crosslegged part 7", 11,
                        R.raw.risset_drum01, R.drawable.wildthing,
                        R.raw.pigeon2, 1);

                InstructionVariant triangleInstructionVariant = new InstructionVariant(2,
                        "triangle01", 2,
                        R.raw.risset_drum01, R.drawable.triangle01,
                        R.raw.boat_pose_paripurna_navasana, 1);
                InstructionVariant fireLogInstructionVariant = new InstructionVariant(3,
                        "firelog01", 3,
                        R.raw.risset_drum01, R.drawable.firelog01,
                        R.raw.child_pose_balasana_yoga_oefeningen, 1);
                InstructionVariant boundAngleInstructionVariant = new InstructionVariant(4,
                        "boundangle01", 4,
                        R.raw.risset_drum01, R.drawable.boundangle01,
                        R.raw.crane_pose_bakasana_, 1);
                InstructionVariant sillyParentTentInstructionVariant = new InstructionVariant(5,
                        "partent01", 5,
                        R.raw.risset_drum01, R.drawable.partent01,
                        R.raw.child_pose_balasana_yoga_oefeningen, 1);

                InstructionVariant mountainInstructionVariant = new InstructionVariant(13,
                        "Mountain Pose", 12,
                        R.raw.risset_drum01, R.drawable.tadasana,
                        R.raw.tadasana, 1);
                InstructionVariant triangleInstructionVariant2 = new InstructionVariant(14,
                        "Triangle Pose 2", 13,
                        R.raw.risset_drum01, R.drawable.triangle,
                        R.raw.triangle, 1);
                InstructionVariant warrior2InstructionVariant = new InstructionVariant(15,
                        "Warrior 2", 14,
                        R.raw.risset_drum01, R.drawable.warrior2right,
                        R.raw.warrior2right, 1);
                InstructionVariant extendedSideAngleInstructionVariant = new InstructionVariant(16,
                        "Extended Side Angle Pose", 15,
                        R.raw.risset_drum01, R.drawable.extside,
                        R.raw.extside, 1);
                InstructionVariant twistedLowLungeInstructionVariant = new InstructionVariant(17,
                        "Revolved Side Angle or Twisted Low Lunge", 16,
                        R.raw.risset_drum01, R.drawable.revolvedsideangle,
                        R.raw.revolvedsideangle, 1);
                InstructionVariant plankInstructionVariant = new InstructionVariant(18,
                        "Plank", 17,
                        R.raw.risset_drum01, R.drawable.plank,
                        R.raw.plank, 1);
                InstructionVariant downDogInstructionVariant = new InstructionVariant(19,
                        "Down Dog Pose", 18,
                        R.raw.risset_drum01, R.drawable.downdog,
                        R.raw.downdog, 1);
                InstructionVariant chaturangaInstructionVariant = new InstructionVariant(20,
                        "Chaturanga", 19,
                        R.raw.risset_drum01, R.drawable.chaturanga,
                        R.raw.chaturanga, 1);
                InstructionVariant upDogInstructionVariant = new InstructionVariant(21,
                        "Up Dog Pose", 20,
                        R.raw.risset_drum01, R.drawable.updog,
                        R.raw.updog, 1);
                InstructionVariant downDogInstructionVariant2 = new InstructionVariant(22,
                        "Down Dog Pose 2", 21,
                        R.raw.risset_drum01, R.drawable.downdog,
                        R.raw.downdog, 1);
                InstructionVariant walkToTopInstructionVariant = new InstructionVariant(23,
                        "Walk To Top", 22,
                        R.raw.risset_drum01, R.drawable.plank,
                        R.raw.plank, 1);
                InstructionVariant rollUpInstructionVariant = new InstructionVariant(24,
                        "Roll Up", 23,
                        R.raw.risset_drum01, R.drawable.tadasana,
                        R.raw.tadasana, 1);
                InstructionVariant mountainInstructionVariant2 = new InstructionVariant(25,
                        "Mountain Pose 2", 24,
                        R.raw.risset_drum01, R.drawable.tadasana,
                        R.raw.tadasana, 1);

                instruction_var_dao.insertInstructionVariants(crossLeggedInstructionVariant,
                        crossLeggedInstructionVariant2, crossLeggedInstruction2Variant2,
                        crossLeggedInstructionVariant3, crossLeggedInstructionVariant4,
                        crossLeggedInstructionVariant5, crossLeggedInstructionVariant6,
                        crossLeggedInstructionVariant7,
                        triangleInstructionVariant, fireLogInstructionVariant,
                        boundAngleInstructionVariant, sillyParentTentInstructionVariant);

                instruction_var_dao.insertInstructionVariants(mountainInstructionVariant,
                        triangleInstructionVariant2, warrior2InstructionVariant,
                        extendedSideAngleInstructionVariant, twistedLowLungeInstructionVariant,
                        plankInstructionVariant, downDogInstructionVariant,
                        chaturangaInstructionVariant, upDogInstructionVariant,
                        downDogInstructionVariant2, walkToTopInstructionVariant,
                        rollUpInstructionVariant, mountainInstructionVariant2);

                mSeqWPoseRepository = new SequenceWithPosesRepository(application);
                // Short Sequence
                List<Pose> poses = Arrays.asList(crossLeggedPose, trianglePose, fireLogPose,
                        boundAnglePose);
                // Create SequencePoseXRef
                mSeqWPoseRepository.composeSequence(shortSequence, poses);

                // Long Sequence
                poses = Arrays.asList(crossLeggedPose, trianglePose, fireLogPose,
                        boundAnglePose, sillyParentTentPose);
                mSeqWPoseRepository.composeSequence(longSequence, poses);

                // Anne's Sequence (can we avoid duplicate stuff?)
                poses = Arrays.asList(mountainPose, trianglePose2, warrior2Pose,
                        extendedSideAnglePose, twistedLowLungePose, plankPose, downDogPose,
                        chaturangaPose, upDogPose, walkToTopPose, rollUpPose, mountainPose2);
                mSeqWPoseRepository.composeSequence(anneSequence, poses);
            });
        }
    };

    private Application mApplication;
    private Context mContext;

    static YogaRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (YogaRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            YogaRoomDatabase.class, "yoga_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        INSTANCE.mContext = context;
        return INSTANCE;
    }

    Application getApplication() {
        return mApplication;
    }

    void setApplication(Application application) {
        mApplication = application;
    }

    // TODO==050 can we use the same dao for multiple things?
    abstract LanguageDao languageDao();

    abstract WordDao wordDao();

    abstract PoseDao poseDao();

    abstract SequencePoseDao sequencePoseDao();

    abstract InstructionTemplateDao instructionTemplateDao();

    abstract InstructionVariantDao instructionVariantDao();

    public abstract TemplateVariantDao templateVariantDao();
}
